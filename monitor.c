#include "system.h"
#include "memlayout.h"
#include "x86.h"
#include "file.h"
#include "spinlock.h"
#include "task.h"

#define CRTPRT 0x3D4

u8 kcursor_x = 0;
u8 kcursor_y = 0;
u16 *kvideo_memory = (u16 *) P2V(0xB8000);


void panic(char *message, char *file, u32 line) {
   asm volatile("cli");
   monitor_write("PANIC(");
   monitor_write(message);
   monitor_write(") at ");
   monitor_write(file);
   monitor_write(":");
   monitor_write_hex(line);
   monitor_write("\n");
   for(;;);
}

void panic_assert(char *file, u32 line, char *desc) {
   asm volatile("cli");
   monitor_write("ASSERTION FAILED(");
   monitor_write(desc);
   monitor_write(") at ");
   monitor_write(file);
   monitor_write(":");
   monitor_write_hex(line);
   monitor_write("\n");
   for(;;);
}


static void move_cursor() {
   u16 location = kcursor_y * 80 + kcursor_x;
   outb(CRTPRT, 14);
   outb(CRTPRT, location >> 8);
   outb(CRTPRT, 15);
   outb(CRTPRT, location);
}

static void scroll() {
   u8 attribute = (0 << 4) | (15 & 0x0F);
   u16 blank = 0x20 | (attribute << 8);
   if(kcursor_y >= 25) {
      int i;
      for(i = 0*80; i < 24*80; i++) {
         kvideo_memory[i] = kvideo_memory[i+80];
      }
      for(i = 24*80; i < 25*80; i++) {
         kvideo_memory[i] = blank;
      }
      kcursor_y = 24;
   }
}

void monitor_clear() {
   u8 attribute = (0 << 4) | (15 & 0x0F);
   u16 blank = 0x20 | (attribute << 8);
   int i; 
   for(i = 0; i < 80*25; i++) {
      kvideo_memory[i] = blank;
   }
   kcursor_y = 0;
   kcursor_x = 0;
   move_cursor();
}

void monitor_put(char c) {
   u8 back_color = 0;
   u8 fore_color = 15;
   u8 attribute = (back_color << 4) | (fore_color & 0x0F);
   u16 attribute_front = attribute << 8;
   u16 *location;
   if(c == 0x08 && kcursor_x) {
      kcursor_x--;
   } 
   else if(c == 0x09) {
      kcursor_x = (kcursor_x+8) & ~(8-1);
   }
   else if(c == '\r') {
      kcursor_x = 0;
   }
   else if(c == '\n') {
      kcursor_x = 0;
      kcursor_y++;
   }
   else if(c >= ' ') {
      location = kvideo_memory + (kcursor_y * 80 + kcursor_x);
      *location = c | attribute_front;
      kcursor_x++;
   }
   if(kcursor_x >= 80) {
      kcursor_x = 0;
      kcursor_y++;
   }
   scroll();
   move_cursor();
}

void monitor_write(char *c) {
   int i = 0; 
   while(c[i]) {
      monitor_put(c[i++]);
   }
}

void monitor_write_dec(u32 n) {
   if (n == 0) {
      monitor_put('0');
      return;
   }

   u32 acc = n;
   char c[32];
   int i = 0;
   while (acc > 0) {
      c[i] = '0' + acc%10;
      acc /= 10;
      i++;
   }
   c[i] = 0;

   char c2[32];
   c2[i--] = 0;
   int j = 0;
   while(i >= 0) {
      c2[i--] = c[j++];
   }
   monitor_write(c2);
}

void monitor_write_hex(u32 n) {
   s16 tmp;
   monitor_write("0x");
   char noZeroes = 1;

   int i;
   for (i = 28; i > 0; i -= 4) {
      tmp = (n >> i) & 0xF;
      if (tmp == 0 && noZeroes != 0) {
         continue;
      }

      if (tmp >= 0xA) {
         noZeroes = 0;
         monitor_put(tmp-0xA+'a' );
      }
      else {
         noZeroes = 0;
         monitor_put( tmp+'0' );
      }
   }

   tmp = n & 0xF;
   if (tmp >= 0xA) {
      monitor_put (tmp-0xA+'a');
   }
   else {
      monitor_put (tmp+'0');
   }

}

void monitor_write_hex_ln(u32 n) {
   monitor_write_hex(n);
   monitor_write("\n");
}

static struct {
   struct spinlock lock;
   int locking;
} cons;


int write_console(struct inode *ip, char *buf, int n) {
   int i;
   unlock_inode(ip);
   acquire(&cons.lock);
   for(i = 0; i < n; i++) {
      monitor_put((char) buf[i] & 0xFF);
   }
   release(&cons.lock);
   lock_inode(ip);
   return n;
}

#define INPUT_BUF 128
struct {
   struct spinlock lock;
   char buf[INPUT_BUF];
   u32 r;
   u32 w;
   u32 e;
} kinput;

#define BACKSPACE 0x100
#define CRTPORT 0x3d4
#define C(x)  ((x)-'@')  // Control-x

static void putc(int c) {
   if(c == BACKSPACE) {
      monitor_put(0x08);
      monitor_put(' ');
      monitor_put(0x08);
   } 
   else {
      monitor_put((char)c);
   }
}


void consoleintr(int (*getc)(void))
{
   int c;

   acquire(&kinput.lock);
   while((c = getc()) >= 0){
      switch(c){
         case C('P'):  // Process listing.
            // procdump();
            break;
         case C('U'):  // Kill line.
            while(kinput.e != kinput.w &&
                  kinput.buf[(kinput.e-1) % INPUT_BUF] != '\n'){
               kinput.e--;
               putc(BACKSPACE);
            }
            break;
         case C('H'): case '\x7f':  // Backspace
            if(kinput.e != kinput.w){
               kinput.e--;
               putc(BACKSPACE);
            }
            break;
         default:
            if(c != 0 && kinput.e-kinput.r < INPUT_BUF){
               c = (c == '\r') ? '\n' : c;
               kinput.buf[kinput.e++ % INPUT_BUF] = c;
               putc(c);
               if(c == '\n' || c == C('D') || kinput.e == kinput.r+INPUT_BUF){
                  kinput.w = kinput.e;
                  wakeup(&kinput.r);
               }
            }
            break;
      }
   }
   release(&kinput.lock);
}

int read_console(struct inode *ip, char *dst, int n) {
   u32 target;
   int c;

   unlock_inode(ip);
   target = n;
   acquire(&kinput.lock);
   while(n > 0){
      while(kinput.r == kinput.w){
         if(kcpu->current_task->killed) {
            release(&kinput.lock);
            lock_inode(ip);
            return -1;
         }
         sleep(&kinput.r, &kinput.lock);
      }
      c = kinput.buf[kinput.r++ % INPUT_BUF];
      if(c == C('D')){  // EOF
         if(n < target) {
            // Save ^D for next time, to make sure
            // caller gets a 0-byte result.
            kinput.r--;
         }
         break;
      }
      *dst++ = c;
      --n;
      if(c == '\n')
         break;
   }
   release(&kinput.lock);
   lock_inode(ip);

   return target - n;
}

extern devsw_t kdevsw[];

void init_monitor() {
   initlock(&cons.lock, "console");
   initlock(&kinput.lock, "input");
   kdevsw[CONSOLE].write = write_console;
   kdevsw[CONSOLE].read = read_console;
   cons.locking = 1;
}

