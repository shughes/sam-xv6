#include "file.h"
#include "param.h"
#include "spinlock.h"
#include "fs.h"

devsw_t kdevsw[NDEV];

struct file_table {
	spinlock_t lock;
	file_t file[NFILE];
} kfile_table;

void init_file() {
	initlock(&kfile_table.lock, "file_table");
}

struct file* alloc_file() {
	struct file *f;
	acquire(&kfile_table.lock);
	for(f = kfile_table.file; f < kfile_table.file + NFILE; f++){
		if(f->ref == 0){
			f->ref = 1;
			release(&kfile_table.lock);
			return f;
		}
	}
	release(&kfile_table.lock);
	return 0;
}

struct file* inc_file_ref(struct file *f) {
	acquire(&kfile_table.lock);
	if(f->ref < 1) {
		PANIC("inc_file_ref");
	}
	f->ref++;
	release(&kfile_table.lock);
	return f;
}

void dec_file_ref(struct file *f) {
	struct file ff;
	acquire(&kfile_table.lock);
	if(f->ref < 1) {
		PANIC("dec_file_ref");
	}
	if(--f->ref > 0) {
		release(&kfile_table.lock);
		return;
	}
	ff = *f;
	f->ref = 0;
	f->type = FD_NODE;
	release(&kfile_table.lock);
  
	/*if(ff.type == FD_PIPE) {
		close_pipe(ff.pipe, ff.writable);
	}
	else */
	if(ff.type == FD_INODE) {
		//begin_op();
		dec_inode_ref(ff.ip);
		//end_op();
	}
}

int get_file_stat(struct file *f, struct stat *st) {
	if(f->type == FD_INODE){
		lock_inode(f->ip);
		copy_inode_stats(f->ip, st);
		unlock_inode(f->ip);
		return 0;
	}
	return -1;
}

int read_file(struct file *f, char *addr, int n) {
	int r;

	if(f->readable == 0) {
		return -1;
	}
	/*if(f->type == FD_PIPE) {
		return read_pipe(f->pipe, addr, n);
		}*/
	if(f->type == FD_INODE){
		lock_inode(f->ip);
		if((r = read_inode(f->ip, addr, f->off, n)) > 0) {
			f->off += r;
		}
		unlock_inode(f->ip);
		return r;
	}
	PANIC("read_file");
	return 0;
}

int write_file(struct file *f, char *addr, int n) {
	int r;

	if(f->writeable == 0) {
		return -1;
	}
	/*if(f->type == FD_PIPE) {
		return write_pipe(f->pipe, addr, n);
		}*/
	if(f->type == FD_INODE) {
		int max = ((LOGSIZE-1-1-2) / 2) * 512;
		int i = 0;
		while(i < n){
			int n1 = n - i;
			if(n1 > max) {
				n1 = max;
			}

			//begin_op();
			lock_inode(f->ip);
			if ((r = write_inode(f->ip, addr + i, f->off, n1)) > 0) {
				f->off += r;
			}
			unlock_inode(f->ip);
			//end_op();

			if(r < 0) {
				break;
			}
			if(r != n1) {
				PANIC("short write_file");
			}
			i += r;
		}
		return i == n ? n : -1;
	}
	PANIC("write_file");
	return 0;
}

