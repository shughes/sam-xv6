#include "syscodes.h"
#include "isr.h"

#define SYSCALL(name) \
  .globl name; \
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(write)
SYSCALL(exec)
SYSCALL(fork)
SYSCALL(exit)
SYSCALL(open)
SYSCALL(mknod)
SYSCALL(dup)
SYSCALL(close)
SYSCALL(wait)
SYSCALL(fstat)
SYSCALL(read)
SYSCALL(sbrk)
SYSCALL(pipe)
SYSCALL(chdir)