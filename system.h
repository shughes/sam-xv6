#ifndef SYSTEM_H
#define SYSTEM_H

#include "types.h"

struct spinlock;
struct page_dir;
struct page_table;
struct page;
struct task;
struct trapframe;
struct cpu;
struct heap;
struct context;
struct buf;
struct stat;
struct inode;

typedef struct page_dir page_dir_t;
typedef struct page_table page_table_t;
typedef struct page page_t;
typedef struct task task_t;
typedef struct trapframe trapframe_t;
typedef struct cpu cpu_t;
typedef struct heap heap_t;
typedef struct context context_t;

#define SIZE(arr) sizeof(arr) / sizeof(arr[0])

// kb.c
void init_kb();

// kheap.c
// void kfree(void *p);
// void free(void *p, heap_t *heap);
char* kalloc_p(u32 size, u32 *phys);
char* kalloc_a(u32 size);
char* kalloc_ap(u32 size, u32 *phys);
char* kalloc(u32 size);

// isr.c
typedef void (*isr_t)(trapframe_t *);
void register_interrupt_handler(u8 n, isr_t handler);

// exec.c
int exec(char *path, char** argv);

// monitor.c
void init_monitor();
void monitor_put(char c);
void monitor_clear();
void monitor_write(char *c);
void monitor_write_hex(u32 n);
void monitor_write_hex_ln(u32 n);
void monitor_write_dec(u32 n);
void panic_assert(char *file, u32 line, char *desc);
void panic(char *message, char *file, u32 line);
void consoleintr(int (*getc)(void));
#define PANIC(msg) panic(msg, __FILE__, __LINE__);
#define ASSERT(msg) ((msg) ? (void) 0 : panic_assert(__FILE__, __LINE__, #msg))

// syscall.c
void init_syscalls();

// task.c
void scheduler();
void sched();
void exit();
void yield();
void init_tasking();
int fork();
int getpid();
void sleep(void *chan, struct spinlock *lock);
void wakeup(void *);
extern cpu_t *kcpu;
int wait();
int grow_task(int n);

// spinlock.c
void initlock(struct spinlock *lock, char *name);
void pushcli();
void popcli();
int holding(struct spinlock *lock);
void acquire(struct spinlock *lock);
void release(struct spinlock *lock);
void getcallerpcs(void *v, u32 pcs[]);

// vm.c
void init_vm();
page_t *get_page(page_dir_t* pgdir, u32 address, int make);
void alloc_frame(page_t* page, int is_kernel, int writeable);
page_dir_t *clone_directory(page_dir_t *src);
void switch_page_directory(page_dir_t *dir);
void init_uvm(page_dir_t* pgdir);
void switch_uvm(struct task *t);
void switch_kvm();
page_dir_t *clone_kernel_directory();
u32 alloc_uvm(page_dir_t *pgdir, u32 old_size, u32 new_size);
int load_uvm(page_dir_t *pgdir, char *addr, struct inode *ip, u32 offset, u32 sz);
void clear_page(page_dir_t *pgdir, char *vaddr);
int copy_out(page_dir_t *pgdir, u32 va, void *ptr, u32 length);
void free_user_page_dir(page_dir_t *pgdir);
int dealloc_uvm(page_dir_t *pgdir, u32 old_size, u32 new_size);

// descriptor_tables.c
void set_kernel_stack(u32 stack);
void init_descriptor_tables();

// timer.c
void init_timer(u32 freq);


// ide.c
void init_ide();
void rw_ide(struct buf *b);

// bcache.c
void init_bcache();
void write_block(struct buf *b);
struct buf *read_block(u32 dev, u32 blockno);
void release_block(struct buf *b);

// fs.c
struct inode* lookup_dir_entry(struct inode *dp, char *name, u32 *poff);
int link_dir(struct inode *dp, char *name, u32 inum);
void dec_inode_ref(struct inode *ip);
void copy_inode_stats(struct inode *ip, struct stat *st);
int write_inode(struct inode *ip, char *src, u32 off, u32 n);
int read_inode(struct inode *ip, char *dst, u32 off, u32 n);
void lock_inode(struct inode *ip);
void copy_inode_stats(struct inode *ip, struct stat *st);
void unlock_inode(struct inode *ip);
struct inode *lookup_parent_inode(char *path, char *name);
struct inode *lookup_inode(char *path);
void unlock_dec_inode_ref(struct inode *ip);
struct inode *alloc_inode(u32 dev, s16 type);
void update_inode(struct inode *ip);
void init_fs();
void copy_inode_stats(struct inode *ip, struct stat *st);
struct inode *inc_inode_ref(struct inode *ip);

// file.c
void init_file();
struct file* alloc_file();
void dec_file_ref(struct file *);
int write_file(struct file *f, char *addr, int n);
int read_file(struct file *f, char *addr, int n);
struct file* inc_file_ref(struct file *f);
int get_file_stat(struct file *f, struct stat *st);

#endif // SYSTEM_H
