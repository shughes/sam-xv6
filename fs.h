#ifndef FS_H
#define FS_H
#include "types.h"
// On-disk file system format. 
// Both the kernel and user programs use this header file.

// Block 0 is unused.
// Block 1 is super block.
// Blocks 2 through sb.ninodes/INODES_PER_BLOCK hold inodes.
// Then free bitmap blocks holding sb.size bits.
// Then sb.nblocks data blocks.
// Then sb.nlog log blocks.

#define ROOTINO 1  // root i-number
#define BLOCK_SIZE 512  // block size

// File system super block
struct superblock {
	u32 size;         // Size of file system image (blocks)
	u32 nblocks;      // Number of data blocks
	u32 ninodes;      // Number of inodes.
	u32 nlog;         // Number of log blocks
};

#define DIRECT_SIZE 12
#define INDIRECT_SIZE (BLOCK_SIZE / sizeof(u32))
#define MAXFILE (DIRECT_SIZE + INDIRECT_SIZE)

// On-disk inode structure
struct disk_inode {
	s16 type;           // File type
	s16 major;          // Major device number (T_DEV only)
	s16 minor;          // Minor device number (T_DEV only)
	s16 nlink;          // Number of links to inode in file system
	u32 size;            // Size of file (bytes)
	u32 addrs[DIRECT_SIZE+1];   // Data block addresses
};

// Inodes per block.
#define INODES_PER_BLOCK           (BLOCK_SIZE / sizeof(struct disk_inode))

// Block containing inode i
#define GET_BLOCK_BY_INODE(i)     ((i) / INODES_PER_BLOCK + 2)

// Bitmap bits per block
#define BITMAP_BITS_PER_BLOCK           (BLOCK_SIZE*8)

// Block containing bit for block b
#define GET_BLOCK_BY_BIT(b, ninodes) (b/BITMAP_BITS_PER_BLOCK + (ninodes)/INODES_PER_BLOCK + 3)

// Directory is a file containing a sequence of dir_entry structures.
#define DIR_LIMIT 14

struct dir_entry {
	u16 inum;
	char name[DIR_LIMIT];
};




#endif // FS_H
