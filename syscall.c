#include "syscall.h"
#include "system.h"
#include "syscodes.h"
#include "x86.h"
#include "task.h"
#include "buf.h"
#include "mmu.h"
#include "filecontrol.h"
#include "stat.h"
#include "file.h"
#include "param.h"

extern devsw_t kdevsw[];

static int argint(int n, trapframe_t *tf) {
	int ptr = (int) tf->esp + 4 + 4*n;
	int *val = (int *) ptr;
	return *val;
}

static char* argstr(int n, trapframe_t *tf) {
	char *str = (char *) argint(n, tf);
	return str;
}

static int argfd(int n, trapframe_t *tf) {
	int fd = argint(n, tf);
	return fd;
}

static struct file* argfile(int n, trapframe_t *tf) {
	int fd = argfd(n, tf);
	return kcpu->current_task->ofile[fd];
}

static struct inode* create(char *path, s16 type, s16 major, s16 minor) {
	u32 off;

	struct inode *ip, *dp;
	char name[DIR_LIMIT];

	if((dp = lookup_parent_inode(path, name)) == 0) {
		return 0;
	}

	lock_inode(dp);

	if((ip = lookup_dir_entry(dp, name, &off)) != 0){
		unlock_dec_inode_ref(dp);
		lock_inode(ip);
		if(type == T_FILE && ip->type == T_FILE) {
			return ip;
		}
		unlock_inode(ip);
		return 0;
	}

	if((ip = alloc_inode(dp->dev, type)) == 0) {
		PANIC("create: alloc_inode");
	}

	lock_inode(ip);
	ip->major = major;
	ip->minor = minor;
	ip->nlink = 1;
	update_inode(ip);

	if(type == T_DIR) { // . and .. directories
		dp->nlink++;  
		update_inode(dp);
		if(link_dir(ip, ".", ip->inum) < 0 || link_dir(ip, "..", dp->inum) < 0) {
			PANIC("create dots");
		}
	}

	if(link_dir(dp, name, ip->inum) < 0) {
		PANIC("create: link_dir");
	}

	unlock_dec_inode_ref(dp);

	return ip;
}

static int fdalloc(struct file *f) {
	int fd;

	for(fd = 0; fd < NOFILE; fd++){
		if(kcpu->current_task->ofile[fd] == 0){
			kcpu->current_task->ofile[fd] = f;
			return fd;
		}
	}
	return -1;
}

int sys_dup() {
	struct file *f = argfile(0, kcpu->current_task->tf);
	int fd = fdalloc(f);
	inc_file_ref(f);
	return fd;
}

int sys_chdir() {
	// begin_op();
	char *path = argstr(0, kcpu->current_task->tf);
	struct inode *ip;
	if((ip = lookup_inode(path)) == 0) {
		// end_op();
		return -1;
	}	
	lock_inode(ip);
	if(ip->type != T_DIR) {
		unlock_dec_inode_ref(ip);
		// end_op();
		return -1;
	}
	unlock_inode(ip);
	dec_inode_ref(kcpu->current_task->cwd);
	// end_op();
	kcpu->current_task->cwd = ip;
	return 0;
}

// TODO: finish pipe
int sys_pipe() {
	return 0;
}

int sys_sbrk() {
	int n = argint(0, kcpu->current_task->tf);
	int addr = kcpu->current_task->size;
	grow_task(n);
	return addr;
}

int sys_wait() {
	return wait();
}

int sys_fstat() {
	struct file *f = argfile(0, kcpu->current_task->tf);
	struct stat *st = (struct stat *) argint(1, kcpu->current_task->tf);
	return get_file_stat(f, st);
}

int sys_mknod() {
	// begin_op();
	char *path = argstr(0, kcpu->current_task->tf);
	int major = argint(1, kcpu->current_task->tf);
	int minor = argint(2, kcpu->current_task->tf);
	struct inode *ip = create(path, T_DEV, major, minor);
	unlock_dec_inode_ref(ip);
	// end_op();
	return 0;
}

int sys_open() {
	char *path;
	int fd, omode;
	struct file *f;
	struct inode *ip;

	path = argstr(0, kcpu->current_task->tf);
	omode = argint(1, kcpu->current_task->tf);

	//begin_op();

	if(omode & O_CREATE) {
		ip = create(path, T_FILE, 0, 0);
		if(ip == 0){
			//end_op();
			return -1;
		}
	} else {
		if((ip = lookup_inode(path)) == 0){
			//end_op();
			return -1;
		}
		lock_inode(ip);
		if(ip->type == T_DIR && omode != O_RDONLY){
			unlock_dec_inode_ref(ip);
			//end_op();
			return -1;
		}
	}

	if((f = alloc_file()) == 0 || (fd = fdalloc(f)) < 0) { 
		if(f) {
			dec_file_ref(f);
		}
		unlock_dec_inode_ref(ip);
		//end_op();
		return -1;
	}
	unlock_inode(ip);
	//end_op();

	f->type = FD_INODE;
	f->ip = ip;
	f->off = 0;
	f->readable = !(omode & O_WRONLY);
	f->writeable = (omode & O_WRONLY) || (omode & O_RDWR);
	
	return fd;	
}

int sys_write() {
	struct file *file = argfile(0, kcpu->current_task->tf);
	int size = argint(2, kcpu->current_task->tf);
	char *str = argstr(1, kcpu->current_task->tf);
	return write_file(file, str, size);
}

int sys_exec() {
	int ptr1 = argint(0, kcpu->current_task->tf);
	int ptr2 = argint(1, kcpu->current_task->tf);
	return exec((char *)ptr1, (char **)ptr2);
}

int sys_fork() {
	return fork();
}

int sys_exit() {
	exit();
	return 0;
}

int sys_close() {
	struct file *f = argfile(0, kcpu->current_task->tf);
	int fd = argfd(0, kcpu->current_task->tf);
	kcpu->current_task->ofile[fd] = 0;
	dec_file_ref(f);
	return 0;
}

int sys_read() {
	struct file *f = argfile(0, kcpu->current_task->tf);
	char *p = argstr(1, kcpu->current_task->tf);
	int n = argint(2, kcpu->current_task->tf);
	return read_file(f, p, n);
}

static int (*ksyscalls[])() = {
	[SYS_write] sys_write,
	[SYS_exec]  sys_exec,
	[SYS_fork]  sys_fork,
	[SYS_exit]  sys_exit,
	[SYS_open]  sys_open,
	[SYS_mknod]	sys_mknod,
	[SYS_dup]	sys_dup,
	[SYS_close]	sys_close,
	[SYS_wait]	sys_wait,
	[SYS_fstat]	sys_fstat,
	[SYS_read]	sys_read,
	[SYS_sbrk]	sys_sbrk,
	[SYS_pipe]	sys_pipe,
	[SYS_chdir]	sys_chdir,
};

static void syscall_handler(trapframe_t *tf) {
	ASSERT(tf->eax < SIZE(ksyscalls));
	kcpu->current_task->tf = tf;
	tf->eax = ksyscalls[tf->eax]();
}

void init_syscalls() {
	register_interrupt_handler(0x80, &syscall_handler);
}
