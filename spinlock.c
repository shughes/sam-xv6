#include "spinlock.h"
#include "system.h"
#include "x86.h"
#include "memlayout.h"
#include "task.h"

extern cpu_t *kcpu;

void initlock(spinlock_t *lock, char *name) {
	lock->name = name;
	lock->locked = 0;
}

void pushcli() {
	cli();
	kcpu->ncli++;
}

void popcli() {
	if(--kcpu->ncli == 0) {
		sti();
	}
}

int holding(spinlock_t *lock) {
	return lock->locked;
}

void acquire(spinlock_t *lock) {
	pushcli();
	if(holding(lock)) {
		PANIC("acquire");
	}
	while(xchg(&lock->locked, 1) != 0) { }
	getcallerpcs(&lock, lock->pcs);
}

void release(spinlock_t *lock) {
	if(!holding(lock)) {
		PANIC("release");
	}
	lock->pcs[0] = 0;
	xchg(&lock->locked, 0);
	popcli();
}

void getcallerpcs(void *v, u32 pcs[]) {
	u32 *ebp;
	int i;
	ebp = (u32 *) v - 2;
	for(i = 0; i < 10; i++) {
		if(ebp == 0 || ebp < (u32 *) KERNBASE || ebp == (u32 *) 0xFFFFFFFF) {
			break;
		}
		pcs[i] = ebp[1];
		ebp = (u32 *) ebp[0];
	}
	for(; i < 10; i++) {
		pcs[i] = 0;
	}
}

