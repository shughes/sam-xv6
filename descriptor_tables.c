#include "descriptor_tables.h"
#include "string.h"
#include "x86.h"
#include "monitor.h"

gdt_ptr_t kgdt_ptr;
gdt_entry_t kgdt_entries[6];
tss_entry_t ktss_entry;


static void gdt_set_gate(int num, u32 base, u32 limit, u8 access, u8 gran) {
	kgdt_entries[num].base_low = base & 0xFFFF;
	kgdt_entries[num].base_middle = (base >> 16) & 0xFF;
	kgdt_entries[num].base_high = (base >> 24) & 0xFF;

	kgdt_entries[num].limit_low = (limit & 0xFFFF);
	kgdt_entries[num].granularity = (limit >> 16) & 0x0F;
	kgdt_entries[num].granularity |= gran & 0xF0;
	kgdt_entries[num].access = access;
}

extern void tss_flush();
extern cpu_t *kcpu;

void write_tss(u32 num, u16 ss0, u32 esp0) {
	u32 base = (u32) &ktss_entry;
	u32 limit = base + sizeof(ktss_entry);
	gdt_set_gate(num, base, limit, 0xE9, 0x00);
	memset(&ktss_entry, 0, sizeof(ktss_entry));
	ktss_entry.ss0 = ss0;
	ktss_entry.esp0 = esp0;
	ktss_entry.cs = 0x0B; // Ring 0 selectors with requested privilege level set to 3
	ktss_entry.ss = ktss_entry.ds = ktss_entry.es = ktss_entry.fs = ktss_entry.gs = 0x13;
}

extern void gdt_flush(u32);

static void init_gdt() {
	kgdt_ptr.limit = (sizeof(gdt_entry_t) * 6) - 1;
	kgdt_ptr.base = (u32) &kgdt_entries;

	gdt_set_gate(0, 0, 0, 0, 0);
	gdt_set_gate(1, 0, 0xFFFFFFFF, 0x9A, 0xCF);
	gdt_set_gate(2, 0, 0xFFFFFFFF, 0x92, 0xCF);
	gdt_set_gate(3, 0, 0xFFFFFFFF, 0xFA, 0xCF);
	gdt_set_gate(4, 0, 0xFFFFFFFF, 0xF2, 0xCF);

	write_tss(5, 0x10, 0x0);

	gdt_flush((u32)&kgdt_ptr);
	tss_flush();
}

void set_kernel_stack(u32 stack) {
	ktss_entry.esp0 = stack;
}

idt_entry_t kidt_entries[256];
idt_ptr_t kidt_ptr;

static void idt_set_gate(u8 num, u32 base, u16 sel, u8 flags) {
	kidt_entries[num].base_lo = base & 0xFFFF;
	kidt_entries[num].base_hi = (base >> 16) & 0xFFFF;
	kidt_entries[num].sel = sel;
	kidt_entries[num].always0 = 0;
	kidt_entries[num].flags = flags | 0x60;
}

extern void idt_flush();


static void init_idt() {
	kidt_ptr.limit = sizeof(idt_entry_t) * 256 - 1;
	kidt_ptr.base = (u32) &kidt_entries;

	memset(&kidt_entries, 0, sizeof(idt_entry_t) * 256);
	int IO_PIC1 = 0x20;
	int IO_PIC2 = 0xA0;

	// remap irq table
	outb(IO_PIC1, 0x11);
	outb(IO_PIC2, 0x11);
	outb(IO_PIC1+1, 0x20);
	outb(IO_PIC2+1, 0x28);
	outb(IO_PIC1+1, 0x04);
	outb(IO_PIC2+1, 0x02);
	outb(IO_PIC1+1, 0x01);
	outb(IO_PIC2+1, 0x01);
	outb(IO_PIC1+1, 0x0);
	outb(IO_PIC2+1, 0x0);


	idt_set_gate( 0, (u32)isr0 , 0x08, 0x8E);
	idt_set_gate( 1, (u32)isr1 , 0x08, 0x8E);
	idt_set_gate( 2, (u32)isr2 , 0x08, 0x8E);
	idt_set_gate( 3, (u32)isr3 , 0x08, 0x8E);
	idt_set_gate( 4, (u32)isr4 , 0x08, 0x8E);
	idt_set_gate( 5, (u32)isr5 , 0x08, 0x8E);
	idt_set_gate( 6, (u32)isr6 , 0x08, 0x8E);
	idt_set_gate( 7, (u32)isr7 , 0x08, 0x8E);
	idt_set_gate( 8, (u32)isr8 , 0x08, 0x8E);
	idt_set_gate( 9, (u32)isr9 , 0x08, 0x8E);
	idt_set_gate(10, (u32)isr10, 0x08, 0x8E);
	idt_set_gate(11, (u32)isr11, 0x08, 0x8E);
	idt_set_gate(12, (u32)isr12, 0x08, 0x8E);
	idt_set_gate(13, (u32)isr13, 0x08, 0x8E);
	idt_set_gate(14, (u32)isr14, 0x08, 0x8E);
	idt_set_gate(15, (u32)isr15, 0x08, 0x8E);
	idt_set_gate(16, (u32)isr16, 0x08, 0x8E);
	idt_set_gate(17, (u32)isr17, 0x08, 0x8E);
	idt_set_gate(18, (u32)isr18, 0x08, 0x8E);
	idt_set_gate(19, (u32)isr19, 0x08, 0x8E);
	idt_set_gate(20, (u32)isr20, 0x08, 0x8E);
	idt_set_gate(21, (u32)isr21, 0x08, 0x8E);
	idt_set_gate(22, (u32)isr22, 0x08, 0x8E);
	idt_set_gate(23, (u32)isr23, 0x08, 0x8E);
	idt_set_gate(24, (u32)isr24, 0x08, 0x8E);
	idt_set_gate(25, (u32)isr25, 0x08, 0x8E);
	idt_set_gate(26, (u32)isr26, 0x08, 0x8E);
	idt_set_gate(27, (u32)isr27, 0x08, 0x8E);
	idt_set_gate(28, (u32)isr28, 0x08, 0x8E);
	idt_set_gate(29, (u32)isr29, 0x08, 0x8E);
	idt_set_gate(30, (u32)isr30, 0x08, 0x8E);
	idt_set_gate(31, (u32)isr31, 0x08, 0x8E);

	idt_set_gate(128, (u32)isr128, 0x08, 0x8E);

	idt_set_gate(32, (u32)irq0, 0x08, 0x8E);
	idt_set_gate(33, (u32)irq1, 0x08, 0x8E);
	idt_set_gate(34, (u32)irq2, 0x08, 0x8E);
	idt_set_gate(35, (u32)irq3, 0x08, 0x8E);
	idt_set_gate(36, (u32)irq4, 0x08, 0x8E);
	idt_set_gate(37, (u32)irq5, 0x08, 0x8E);
	idt_set_gate(38, (u32)irq6, 0x08, 0x8E);
	idt_set_gate(39, (u32)irq7, 0x08, 0x8E);
	idt_set_gate(40, (u32)irq8, 0x08, 0x8E);
	idt_set_gate(41, (u32)irq9, 0x08, 0x8E);
	idt_set_gate(42, (u32)irq10, 0x08, 0x8E);
	idt_set_gate(43, (u32)irq11, 0x08, 0x8E);
	idt_set_gate(44, (u32)irq12, 0x08, 0x8E);
	idt_set_gate(45, (u32)irq13, 0x08, 0x8E);
	idt_set_gate(46, (u32)irq14, 0x08, 0x8E);
	idt_set_gate(47, (u32)irq15, 0x08, 0x8E);

	idt_flush((u32) &kidt_ptr);
}

extern isr_t kinterrupt_handlers[];
void init_descriptor_tables() {
	init_gdt();
	init_idt();
	memset(&kinterrupt_handlers, 0, sizeof(isr_t)*256);
}
