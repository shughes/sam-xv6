#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <assert.h>

#define stat xv6_stat  // avoid clash with host struct stat
#include "types.h"
#include "fs.h"
#include "stat.h"
#include "param.h"

#define my_static_assert(a, b) do { switch (0) case 0: case (a): ; } while (0)

#define NINODES 200

// Disk layout:
// [ boot block | sb block | inode blocks | bit map | data blocks | log ]

int nbitmap = FS_SIZE_IN_BLOCKS/(BLOCK_SIZE*8) + 1;
int ninodeblocks = NINODES / INODES_PER_BLOCK + 1;
int nlog = LOGSIZE;  
int nmeta;    // Number of meta blocks (inode, bitmap, and 2 extra)
int nblocks;  // Number of data blocks

int fsfd;
struct superblock sb;
char zeroes[BLOCK_SIZE];
u32 freeinode = 1;
u32 freeblock;


void alloc_block(int);
void write_sector(u32, void*);
void write_inode(u32, struct disk_inode*);
void read_inode(u32 inum, struct disk_inode *ip);
void read_sector(u32 sec, void *buf);
u32 alloc_inode(u16 type);
void append_inode(u32 inum, void *p, int n);

// convert to intel byte order
u16
xshort(u16 x)
{
	u16 y;
	u8 *a = (u8*)&y;
	a[0] = x;
	a[1] = x >> 8;
	return y;
}

u32
xint(u32 x)
{
	u32 y;
	u8 *a = (u8*)&y;
	a[0] = x;
	a[1] = x >> 8;
	a[2] = x >> 16;
	a[3] = x >> 24;
	return y;
}

int
main(int argc, char *argv[])
{
	int i, cc, fd;
	u32 rootino, inum, off;
	struct dir_entry de;
	char buf[BLOCK_SIZE];
	struct disk_inode din;

	my_static_assert(sizeof(int) == 4, "Integers must be 4 bytes!");

	if(argc < 2){
		fprintf(stderr, "Usage: mkfs fs.img files...\n");
		exit(1);
	}

	assert((BLOCK_SIZE % sizeof(struct disk_inode)) == 0);
	assert((BLOCK_SIZE % sizeof(struct dir_entry)) == 0);

	fsfd = open(argv[1], O_RDWR|O_CREAT|O_TRUNC, 0666);
	if(fsfd < 0){
		perror(argv[1]);
		exit(1);
	}

	nmeta = 2 + ninodeblocks + nbitmap;
	nblocks = FS_SIZE_IN_BLOCKS - nlog - nmeta;

	sb.size = xint(FS_SIZE_IN_BLOCKS);
	sb.nblocks = xint(nblocks); // so whole disk is size sectors
	sb.ninodes = xint(NINODES);
	sb.nlog = xint(nlog);

	printf("nmeta %d (boot, super, inode blocks %u, bitmap blocks %u) blocks %d log %u total %d\n", 
		   nmeta, ninodeblocks, nbitmap, nblocks, nlog, FS_SIZE_IN_BLOCKS);

	freeblock = nmeta;     // the first free block that we can allocate

	for(i = 0; i < FS_SIZE_IN_BLOCKS; i++) {
		write_sector(i, zeroes);
	}

	memset(buf, 0, sizeof(buf));
	memmove(buf, &sb, sizeof(sb));
	// write super block
	write_sector(1, buf);

	rootino = alloc_inode(T_DIR);
	assert(rootino == ROOTINO);

	bzero(&de, sizeof(de));
	de.inum = xshort(rootino);
	strcpy(de.name, ".");
	append_inode(rootino, &de, sizeof(de));

	bzero(&de, sizeof(de));
	de.inum = xshort(rootino);
	strcpy(de.name, "..");
	append_inode(rootino, &de, sizeof(de));

	for(i = 2; i < argc; i++){
		assert(index(argv[i], '/') == 0);

		if((fd = open(argv[i], 0)) < 0){
			perror(argv[i]);
			exit(1);
		}
    
		// Skip leading _ in name when writing to file system.
		// The binaries are named _rm, _cat, etc. to keep the
		// build operating system from trying to execute them
		// in place of system binaries like rm and cat.
		if(argv[i][0] == '_') {
			++argv[i];
		}

		inum = alloc_inode(T_FILE);

		bzero(&de, sizeof(de));
		de.inum = xshort(inum);
		strncpy(de.name, argv[i], DIR_LIMIT);
		append_inode(rootino, &de, sizeof(de));

		while((cc = read(fd, buf, sizeof(buf))) > 0) {
			append_inode(inum, buf, cc);
		}

		close(fd);
	}

	// fix size of root inode dir
	read_inode(rootino, &din);
	off = xint(din.size);
	off = ((off/BLOCK_SIZE) + 1) * BLOCK_SIZE;
	din.size = xint(off);
	write_inode(rootino, &din);

	alloc_block(freeblock);

	exit(0);
}

void
write_sector(u32 sec, void *buf)
{
	if(lseek(fsfd, sec * BLOCK_SIZE, 0) != sec * BLOCK_SIZE){
		perror("lseek");
		exit(1);
	}
	if(write(fsfd, buf, BLOCK_SIZE) != BLOCK_SIZE){
		perror("write");
		exit(1);
	}
}

void
write_inode(u32 inum, struct disk_inode *ip)
{
	char buf[BLOCK_SIZE];
	u32 bn;
	struct disk_inode *dip;

	bn = GET_BLOCK_BY_INODE(inum);
	read_sector(bn, buf);
	dip = ((struct disk_inode*)buf) + (inum % INODES_PER_BLOCK);
	*dip = *ip;
	write_sector(bn, buf);
}

void
read_inode(u32 inum, struct disk_inode *ip)
{
	char buf[BLOCK_SIZE];
	u32 bn;
	struct disk_inode *dip;

	bn = GET_BLOCK_BY_INODE(inum);
	read_sector(bn, buf);
	dip = ((struct disk_inode*)buf) + (inum % INODES_PER_BLOCK);
	*ip = *dip;
}

void
read_sector(u32 sec, void *buf)
{
	if(lseek(fsfd, sec * BLOCK_SIZE, 0) != sec * BLOCK_SIZE){
		perror("lseek");
		exit(1);
	}
	if(read(fsfd, buf, BLOCK_SIZE) != BLOCK_SIZE){
		perror("read");
		exit(1);
	}
}

u32
alloc_inode(u16 type)
{
	u32 inum = freeinode++;
	struct disk_inode din;

	bzero(&din, sizeof(din));
	din.type = xshort(type);
	din.nlink = xshort(1);
	din.size = xint(0);
	write_inode(inum, &din);
	return inum;
}

void
alloc_block(int used)
{
	u8 buf[BLOCK_SIZE];
	int i;

	printf("alloc_block: first %d blocks have been allocated\n", used);
	assert(used < BLOCK_SIZE*8);
	bzero(buf, BLOCK_SIZE);
	for(i = 0; i < used; i++){
		buf[i/8] = buf[i/8] | (0x1 << (i%8));
	}
	printf("alloc_block: write bitmap block at sector %d\n", ninodeblocks+2);
	write_sector(ninodeblocks+2, buf);
}

#define min(a, b) ((a) < (b) ? (a) : (b))

void
append_inode(u32 inum, void *xp, int n)
{
	char *p = (char*)xp;
	u32 fbn, off, n1;
	struct disk_inode din;
	char buf[BLOCK_SIZE];
	u32 indirect[INDIRECT_SIZE];
	u32 x;

	read_inode(inum, &din);

	off = xint(din.size);
	while(n > 0){
		fbn = off / BLOCK_SIZE;
		assert(fbn < MAXFILE);
		if(fbn < DIRECT_SIZE){
			if(xint(din.addrs[fbn]) == 0){
				din.addrs[fbn] = xint(freeblock++);
			}
			x = xint(din.addrs[fbn]);
		} else {
			if(xint(din.addrs[DIRECT_SIZE]) == 0){
				// printf("allocate indirect block\n");
				din.addrs[DIRECT_SIZE] = xint(freeblock++);
			}
			// printf("read indirect block\n");
			read_sector(xint(din.addrs[DIRECT_SIZE]), (char*)indirect);
			if(indirect[fbn - DIRECT_SIZE] == 0){
				indirect[fbn - DIRECT_SIZE] = xint(freeblock++);
				write_sector(xint(din.addrs[DIRECT_SIZE]), (char*)indirect);
			}
			x = xint(indirect[fbn-DIRECT_SIZE]);
		}
		n1 = min(n, (fbn + 1) * BLOCK_SIZE - off);
		read_sector(x, buf);
		bcopy(p, buf + off - (fbn * BLOCK_SIZE), n1);
		// printf("sector %d, buf %s\n", x, buf);
		write_sector(x, buf);
		n -= n1;
		off += n1;
		p += n1;
	}
	din.size = xint(off);
	write_inode(inum, &din);
}
