#include "system.h"
#include "x86.h"
#include "buf.h"
#include "isr.h"
#include "spinlock.h"
#include "fs.h"


#define SECTOR_SIZE 512
#define IDE_BSY 0x80
#define IDE_DRDY 0x40
#define IDE_DF 0x20
#define IDE_ERR 0x1

#define IDE_CMD_READ  0x20
#define IDE_CMD_WRITE 0x30

static int havedisk1 = 0;
static struct spinlock idelock;
static struct buf *idequeue;

static int wait_ide(int checkerr) {
	int r;
	while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY) { }
	if(checkerr && (r & (IDE_DF | IDE_ERR)) !=0) {
		return -1;
	}
	return 0;
}

static void start_ide(struct buf* b) {
	int sector_per_block = BLOCK_SIZE / SECTOR_SIZE;
	int sector = b->blockno * sector_per_block;

	wait_ide(0);
	outb(0x3f6, 0);  // generate interrupt
	outb(0x1f2, sector_per_block);  // number of sectors
	outb(0x1f3, sector & 0xff);
	outb(0x1f4, (sector >> 8) & 0xff);
	outb(0x1f5, (sector >> 16) & 0xff);
	outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));

	if(b->flags & B_DIRTY) {
		outb(0x1f7, IDE_CMD_WRITE);
		outsl(0x1f0, b->data, BLOCK_SIZE/4);
	} else {
		outb(0x1f7, IDE_CMD_READ);
	}
}

void rw_ide(struct buf *b) {
	struct buf **pp;
	acquire(&idelock);
	b->qnext = 0;
	for(pp = &idequeue; *pp; pp = &(*pp)->qnext) { 
	}
	*pp = b;
	if(idequeue == b) {
		start_ide(b);
	}
	while((b->flags & (B_VALID|B_DIRTY)) != B_VALID) {
		sleep(b, &idelock);
	}
	release(&idelock);
}

static void ide_interrupt(trapframe_t *tf) {
	struct buf *b;
	acquire(&idelock);
	if((b = idequeue) == 0) {
		release(&idelock);
		return;
	}
	idequeue = b->qnext;
	if(!(b->flags & B_DIRTY) && wait_ide(1) >= 0) {
		insl(0x1f0, b->data, BLOCK_SIZE/4);
	}
	b->flags |= B_VALID;
	b->flags &= ~B_DIRTY;
	wakeup(b);
	if(idequeue != 0) {
		start_ide(idequeue);
	}
	release(&idelock);
}

void init_ide() {
	initlock(&idelock, "ide");
	register_interrupt_handler(IRQ0+IRQ_IDE, ide_interrupt);
	wait_ide(0);
	int i;
	
	// Check if disk 1 is present
	outb(0x1f6, 0xe0 | (1<<4));
	for(i=0; i < 1000; i++) {
		if(inb(0x1f7) != 0) {
			havedisk1 = 1;
			break;
		}
	}

	// Switch back to disk 0
	outb(0x1f6, 0xe0 | (0<<4));
}

