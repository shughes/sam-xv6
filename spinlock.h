#ifndef SPINLOCK_H
#define SPINLOCK_H

#include "system.h"

typedef struct spinlock {
	u32 locked;
	char *name;
	u32 pcs[10];
} spinlock_t;


#endif // SPINLOCK_H
