#ifndef ORDERED_ARRAY_H
#define ORDERED_ARRAY_H

#include "types.h"

typedef void* type_t;
typedef char (*lessthan_pred_t)(type_t, type_t);

typedef struct {
	type_t *array;
	u32 size;
	u32 max_size;
	lessthan_pred_t less_than;
} ordered_array_t;

char standard_lessthan_pred(type_t a, type_t b);

ordered_array_t create_ordered_array(u32 max_size, lessthan_pred_t less_than);
ordered_array_t place_ordered_array(void *addr, u32 max_size, lessthan_pred_t less_than);
void insert_ordered_array(type_t item, ordered_array_t *array);
type_t lookup_ordered_array(u32 i, ordered_array_t *array);
void remove_ordered_array(u32 i, ordered_array_t *array);


#endif // ORDERED_ARRAY_H
