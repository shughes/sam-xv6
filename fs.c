#include "fs.h"
#include "system.h"
#include "spinlock.h"
#include "param.h"
#include "file.h"
#include "string.h"
#include "buf.h"
#include "stat.h"
#include "file.h"
#include "task.h"

#define min(a, b) ((a) < (b) ? (a) : (b))

void read_sb(int dev, struct superblock *sb) {
	struct buf *bp;
	bp = read_block(dev, 1);
	memmove(sb, bp->data, sizeof(*sb));
	release_block(bp);
}

static void zero_block(int dev, int blockno) {
	struct buf *bp;
	bp = read_block(dev, blockno);
	memset(bp->data, 0, BLOCK_SIZE);
	// log_write()
	write_block(bp);
	release_block(bp);
}

u32 alloc_block(u32 dev) {
	int b, bi, m;
	struct buf *bp;
	struct superblock sb;
	bp = 0;
	read_sb(dev, &sb);
	for(b = 0; b < sb.size; b += BITMAP_BITS_PER_BLOCK) {
		bp = read_block(dev, GET_BLOCK_BY_BIT(b, sb.ninodes));
		for(bi = 0; bi < BITMAP_BITS_PER_BLOCK && b + bi < sb.size; bi++) {
			m = 1 << (bi % 8);
			if((bp->data[bi/8] & m) == 0) {
				bp->data[bi/8] |= m;
				// log_write(bp);
				write_block(bp);
				release_block(bp);
				zero_block(dev, b + bi);
				return b + bi;
			}
		}
		release_block(bp);
	}
	PANIC("alloc_block: out of blocks");
	return 0;
}

void free_block(int dev, u32 b) {
	struct buf *bp;
	struct superblock sb;
	int bi, m;
	read_sb(dev, &sb);
	bp = read_block(dev, GET_BLOCK_BY_BIT(b, sb.ninodes));
	bi = b % BITMAP_BITS_PER_BLOCK;
	m = 1 << (bi % 8);
	if((bp->data[bi/8] & m) == 0) {
		PANIC("freeing free block");
	}
	bp->data[bi/8] &= ~m;
	// log_write(bp);
	write_block(bp);
	release_block(bp);
}

static u32 map_block(struct inode *ip, u32 bn) {
	u32 addr, *a;
	struct buf *bp;

	if(bn < DIRECT_SIZE){
		if((addr = ip->addrs[bn]) == 0) {
			ip->addrs[bn] = addr = alloc_block(ip->dev);
		}
		return addr;
	}
	
	bn -= DIRECT_SIZE;

	if(bn < INDIRECT_SIZE) {
		if((addr = ip->addrs[DIRECT_SIZE]) == 0) {
			ip->addrs[DIRECT_SIZE] = addr = alloc_block(ip->dev);
		}
		bp = read_block(ip->dev, addr);
		a = (u32*) bp->data;
		if((addr = a[bn]) == 0) {
			a[bn] = addr = alloc_block(ip->dev);
			//log_write(bp);
			write_block(bp);
		}
		release_block(bp);
		return addr;
	}

	PANIC("map_block: out of range");
	return 0;
}

struct {
	spinlock_t lock;
	struct inode inode[NINODE];
} icache;

void init_fs() {
	initlock(&icache.lock, "icache");
}

static struct inode *get_cached_inode(u32 dev, u32 inum) {
	struct inode *ip, *empty;
	acquire(&icache.lock);
	empty = 0;
	for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++) {
		if(ip->ref > 0 && ip->dev == dev && ip->inum == inum) {
			ip->ref++;
			release(&icache.lock);
			return ip;
		}
		if(empty == 0 && ip->ref == 0) {
			empty = ip;
		}
	}
	if(empty == 0) {
		PANIC("get_cached_inode: no inodes");
	}
	ip = empty;
	ip->dev = dev;
	ip->inum = inum;
	ip->ref = 1;
	ip->flags = 0;
	release(&icache.lock);
	return ip;
}

struct inode *alloc_inode(u32 dev, s16 type) {
	int inum;
	struct buf *bp;
	struct disk_inode *dip;
	struct superblock sb;
	read_sb(dev, &sb);
	for(inum = 1; inum < sb.ninodes; inum++) {
		bp = read_block(dev, GET_BLOCK_BY_INODE(inum));
		dip = (struct disk_inode *)bp->data + inum % INODES_PER_BLOCK;
		if(dip->type == 0) {
			memset(dip, 0, sizeof(*dip));
			dip->type = type;
			// log_write(bp);
			write_block(bp);
			release_block(bp);
			return get_cached_inode(dev, inum);
		}
		release_block(bp);
	}
	PANIC("alloc_inode: no inodes");
	return 0;
}


void update_inode(struct inode *ip) {
	struct buf *bp;
	struct disk_inode *dip;
	bp = read_block(ip->dev, GET_BLOCK_BY_INODE(ip->inum));
	dip = (struct disk_inode*)bp->data + ip->inum % INODES_PER_BLOCK;
	dip->type = ip->type;
	dip->major = ip->major;
	dip->minor = ip->minor;
	dip->nlink = ip->nlink;
	dip->size = ip->size;
	memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
	// log_write(bp);
	write_block(bp);
	release_block(bp);
}

struct inode *inc_inode_ref(struct inode *ip) {
	acquire(&icache.lock);
	ip->ref++;
	release(&icache.lock);
	return ip;
}

void lock_inode(struct inode *ip) {
	struct buf *bp;
	struct disk_inode *dip;

	if(ip == 0 || ip->ref < 1) {
		PANIC("lock_inode");
	}
	acquire(&icache.lock);
	while(ip->flags & I_BUSY) {
		sleep(ip, &icache.lock);
	}
	ip->flags |= I_BUSY;
	release(&icache.lock);

	if(!(ip->flags & I_VALID)) {
		bp = read_block(ip->dev, GET_BLOCK_BY_INODE(ip->inum));
		dip = (struct disk_inode*) bp->data + ip->inum % INODES_PER_BLOCK;
		ip->type = dip->type;
		ip->major = dip->major;
		ip->minor = dip->minor;
		ip->nlink = dip->nlink;
		ip->size = dip->size;
		memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
		release_block(bp);
		ip->flags |= I_VALID;
		if(ip->type == 0) {
			PANIC("lock_inode: no type");
		}
	}
}

void unlock_inode(struct inode *ip) {
	if(ip == 0 || !(ip->flags & I_BUSY) || ip->ref < 1) {
		PANIC("unlock_inode");
	}
	acquire(&icache.lock);
	ip->flags &= ~I_BUSY;
	wakeup(ip);
	release(&icache.lock);
}

static void truncate_inode(struct inode *ip) {
	int i, j;
	struct buf *bp;
	u32 *a;
	for(i = 0; i < DIRECT_SIZE; i++) {
		if(ip->addrs[i]) {
			free_block(ip->dev, ip->addrs[i]);
			ip->addrs[i] = 0;
		}
	}
	if(ip->addrs[DIRECT_SIZE]) {
		bp = read_block(ip->dev, ip->addrs[DIRECT_SIZE]);
		a = (u32*) bp->data;
		for(j = 0; j < INDIRECT_SIZE; j++) {
			if(a[j]) {
				free_block(ip->dev, a[j]);
			}
		}
		release_block(bp);
		free_block(ip->dev, ip->addrs[DIRECT_SIZE]);
		ip->addrs[DIRECT_SIZE] = 0;
	}
	ip->size = 0;
	update_inode(ip);
}

void dec_inode_ref(struct inode *ip) {
	acquire(&icache.lock);
	if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0) {
		if(ip->flags & I_BUSY) {
			PANIC("dec_inode_ref: busy");
		}
		ip->flags |= I_BUSY;
		release(&icache.lock);
		truncate_inode(ip);
		ip->type = 0;
		update_inode(ip);
		acquire(&icache.lock);
		ip->flags = 0;
		wakeup(ip);
	}
	ip->ref--;
	release(&icache.lock);
}

void copy_inode_stats(struct inode *ip, struct stat *st) {
	st->dev = ip->dev;
	st->ino = ip->inum;
	st->type = ip->type;
	st->nlink = ip->nlink;
	st->size = ip->size;
}

int write_inode(struct inode *ip, char *src, u32 off, u32 n) {
	u32 tot, m;
	struct buf *bp;

	if(ip->type == T_DEV){
		if(ip->major < 0 || ip->major >= NDEV || !kdevsw[ip->major].write) {
			return -1;
		}
		return kdevsw[ip->major].write(ip, src, n);
	}

	if(off > ip->size || off + n < off) {
		return -1;
	}

	if(off + n > MAXFILE*BLOCK_SIZE) {
		return -1;
	}

	for(tot=0; tot<n; tot+=m, off+=m, src+=m) {
		bp = read_block(ip->dev, map_block(ip, off/BLOCK_SIZE));
		m = min(n - tot, BLOCK_SIZE - off%BLOCK_SIZE);
		memmove(bp->data + off % BLOCK_SIZE, src, m);
		// log_write(bp);
		write_block(bp);
		release_block(bp);
	}

	if(n > 0 && off > ip->size){
		ip->size = off;
		update_inode(ip);
	}
	return n;
}

int read_inode(struct inode *ip, char *dst, u32 off, u32 n) {
	u32 tot, m;
	struct buf *bp;

	if(ip->type == T_DEV) {
		if(ip->major < 0 || ip->major >= NDEV || !kdevsw[ip->major].read) {
			return -1;
		}
		return kdevsw[ip->major].read(ip, dst, n);
	}

	if(off > ip->size || off + n < off) {
		return -1;
	}
	if(off + n > ip->size) {
		n = ip->size - off;
	}

	for(tot=0; tot<n; tot+=m, off+=m, dst+=m) {
		u32 num = map_block(ip, off/BLOCK_SIZE);
		bp = read_block(ip->dev, num);
		m = min(n - tot, BLOCK_SIZE - off%BLOCK_SIZE);
		memmove(dst, bp->data + off%BLOCK_SIZE, m);
		release_block(bp);
	}
	return n;
}

int namecmp(const char *s, const char *t) {
	return strncmp(s, t, DIR_LIMIT);
}

int link_dir(struct inode *dp, char *name, u32 inum) {
	int off;
	struct dir_entry de;
	struct inode *ip;

	// Check that name is not present.
	if((ip = lookup_dir_entry(dp, name, 0)) != 0){
		dec_inode_ref(ip);
		return -1;
	}

	// Look for an empty dir_entry.
	for(off = 0; off < dp->size; off += sizeof(de)) {
		if(read_inode(dp, (char*)&de, off, sizeof(de)) != sizeof(de)) {
			PANIC("link_dir read");
		}
		if(de.inum == 0) {
			break;
		}
	}
	
	strncpy(de.name, name, DIR_LIMIT);
	de.inum = inum;
	if(write_inode(dp, (char*)&de, off, sizeof(de)) != sizeof(de)) {
		PANIC("link_dir");
	}

	return 0;
}


struct inode* lookup_dir_entry(struct inode *dp, char *name, u32 *poff) {
	u32 off, inum;
	struct dir_entry de;

	if(dp->type != T_DIR) {
		PANIC("lookup_dir_entry not DIR");
	}

	for(off = 0; off < dp->size; off += sizeof(de)) {
		if(read_inode(dp, (char *) &de, off, sizeof(de)) != sizeof(de)) {
			PANIC("link_dir read");
		}
		if(de.inum == 0) {
			continue;
		}
		if(namecmp(name, de.name) == 0){
			// entry matches path element
			if(poff) {
				*poff = off;
			}
			inum = de.inum;
			return get_cached_inode(dp->dev, inum);
		}
	}

	return 0;
}

void unlock_dec_inode_ref(struct inode *ip) {
	unlock_inode(ip);
	dec_inode_ref(ip);
}

static char *skip_elem(char *path, char *name) {
	char *s;
	int len;

	while(*path == '/') {
		path++;
	}
	if(*path == 0) {
		return 0;
	}
	s = path;
	while(*path != '/' && *path != 0) {
		path++;
	}
	len = path - s;
	if(len >= DIR_LIMIT) {
		memmove(name, s, DIR_LIMIT);
	}
	else {
		memmove(name, s, len);
		name[len] = 0;
	}
	while(*path == '/') {
		path++;
	}
	return path;
}

extern cpu_t *kcpu;

static struct inode *_lookup_inode(char *path, int nameiparent, char *name) {
	struct inode *ip, *next;

	if(*path == '/') {
		ip = get_cached_inode(ROOTDEV, ROOTINO);
	}
	else {
		ip = inc_inode_ref(kcpu->current_task->cwd);
	}

	while((path = skip_elem(path, name)) != 0) {
		lock_inode(ip);
		if(ip->type != T_DIR){
			unlock_dec_inode_ref(ip);
			return 0;
		}
		if(nameiparent && *path == '\0'){
			// Stop one level early.
			unlock_inode(ip);
			return ip;
		}
		if((next = lookup_dir_entry(ip, name, 0)) == 0){
			unlock_dec_inode_ref(ip);
			return 0;
		}
		unlock_dec_inode_ref(ip);
		ip = next;
	}
	if(nameiparent){
		dec_inode_ref(ip);
		return 0;
	}
	return ip;
}

struct inode *lookup_inode(char *path) {
	char name[DIR_LIMIT];
	return _lookup_inode(path, 0, name);
}

struct inode *lookup_parent_inode(char *path, char *name) {
	return _lookup_inode(path, 1, name);
}

