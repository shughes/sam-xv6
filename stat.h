#ifndef STAT_H
#define STAT_H

#define T_DIR  1   // Directory
#define T_FILE 2   // File
#define T_DEV  3   // Device

struct stat {
	s16 type;  // Type of file
	int dev;     // File system's disk device
	u32 ino;    // Inode number
	s16 nlink; // Number of links to file
	u32 size;   // Size of file in bytes
};


#endif // STAT_H
