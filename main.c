#include "system.h"
#include "types.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "kb.h"
#include "buf.h"


int main() {
	init_descriptor_tables();
	
	monitor_clear();
	
	init_vm();
	init_tasking();
	init_syscalls();
	init_kb();
	init_bcache();
	init_ide();
	init_file();
	init_fs();
	init_monitor();
	init_timer(50);
			
	scheduler();
	
	return 0;
}

__attribute__((__aligned__(PGSIZE)))
pde_t entrypgdir[NPDENTRIES] = {
	[0] = (0) | PTE_P | PTE_W | PTE_PS,
	[KERNBASE>>PDXSHIFT] = (0) | PTE_P | PTE_W | PTE_PS,
};
