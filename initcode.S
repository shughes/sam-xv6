# Initial process execs /init.

#include "syscodes.h"
#include "filecontrol.h"

# exec(init, argv)
.globl start
start:
	/* movl $SYS_open, %eax
	pushl $O_RDWR
	pushl $console
	pushl $0
	int $0x80
	addl $12, %esp
	cmpl $-1, %eax
	jne print_it 		// skip mknod if already exists

	movl $SYS_mknod, %eax
	pushl $1			// minor 
	pushl $1 			// major (equal to CONSOLE in file.h)
	pushl $console
	pushl $0
	int $0x80
	addl $16, %esp

	movl $SYS_open, %eax
	pushl $O_RDWR
	pushl $console
	pushl $0
	int $0x80 
	addl $12, %esp

print_it:
	pushl $6
	pushl $init
	pushl $0
	pushl $0 
	movl $SYS_write, %eax
	int $0x80
	addl $16, %esp */
	
	pushl $argv
	pushl $init
	pushl $0  // where caller pc would be
	movl $SYS_exec, %eax
	int $0x80
	addl $16, %esp

# for(;;) exit();
exit:
	movl $SYS_exit, %eax
	int $0x80
	jmp exit

space:
	.string " "

console:
	.string "console\0"

my_file:
	.string "/README.md\0"

# char init[] = "/init\0";
init:
  .string "/init\0"

# char *argv[] = { init, 0 };
.p2align 2
argv:
  .long init
  .long 0

