#include "timer.h"
#include "system.h"
#include "x86.h"
#include "task.h"
#include "isr.h"

int tick = 0;
extern cpu_t *kcpu;

static void timer_callback(trapframe_t *tf) {
	if(kcpu->current_task && kcpu->current_task->state == RUNNING) {
		yield();
	}
}

void init_timer(u32 freq) {
	register_interrupt_handler(IRQ0, &timer_callback);
	u32 divisor = 1193180 / freq;
	outb(0x43, 0x36);
	u8 l = (u8) (divisor & 0xFF);
	u8 h = (u8) ((divisor >> 8) & 0xFF);
	outb(0x40, l);
	outb(0x40, h);
}
