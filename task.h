#ifndef TASK_H
#define TASK_H

#include "system.h"
#include "x86.h"
#include "spinlock.h"
#include "param.h"
#include "file.h"


#define KERNEL_STACK_SIZE 2048

enum procstate { UNUSED, EMBRYO, SLEEPING, RUNNABLE, RUNNING, ZOMBIE };

struct context {
	u32 edi;
	u32 esi;
	u32 ebx;
	u32 ebp;
	u32 eip;
};

struct task {
	int id;
	enum procstate state;
	struct context *context;
	page_dir_t *page_dir;
	struct task *next;
	struct task *parent;
	u32 kernel_stack;
	void *chan;
	trapframe_t *tf;
	struct inode *cwd;
	struct file *ofile[NOFILE];
	int killed;
	u32 size;
   char name[16];
};

struct cpu {
	task_t *current_task;
	task_t *task_table;
	task_t *init_task;
	context_t *sched_context;
	struct spinlock lock;
	int ncli;
	u32 next_pid;
};


#endif // TASK_H
