#include "system.h"
#include "file.h"
#include "elf.h"
#include "mmu.h"
#include "vm.h"
#include "param.h"
#include "string.h"
#include "task.h"

int exec(char *path, char **argv) {
	// begin_op();
	struct inode *ip = lookup_inode(path);
	lock_inode(ip);
	
	// ELF Header
	struct elfhdr elf;
	if(read_inode(ip, (char *) &elf, 0, sizeof(elf)) < sizeof(elf)) {
		PANIC("exec: elf size");
	}
	if(elf.magic != ELF_MAGIC) {
		PANIC("exec: elf magic");
	}

	page_dir_t *pgdir = clone_kernel_directory();
	struct proghdr ph;
	int i, off;
	u32 sz = 0;
	for(i = 0, off = elf.phoff; i < elf.phnum; i++, off += sizeof(ph)) {
		if(read_inode(ip, (char *)&ph, off, sizeof(ph)) != sizeof(ph)) {
			PANIC("exec: program size");
		}
		if(ph.type != ELF_PROG_LOAD) {
			continue;
		}
		if(ph.memsz < ph.filesz) {
			PANIC("exec: mem size");
		}
		sz = alloc_uvm(pgdir, sz, ph.vaddr + ph.memsz);
		load_uvm(pgdir, (char *)ph.vaddr, ip, ph.off, ph.filesz);
	}
	unlock_dec_inode_ref(ip);
	// end_op();
	ip = 0;

	sz = PAGE_ROUND_UP(sz);
	sz = alloc_uvm(pgdir, sz, sz + 2 * PGSIZE);
	clear_page(pgdir, (char *) (sz - 2 * PGSIZE));
	u32 sp = sz;

	u32 argc, ustack[3+MAXARG+1];
	for(argc = 0; argv[argc]; argc++) {
		ASSERT(argc < MAXARG);
		sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
		copy_out(pgdir, sp, argv[argc], strlen(argv[argc])+1);
		ustack[3+argc] = sp;
	}
	ustack[3+argc] = 0;
	ustack[0] = 0xffffffff;  // fake return PC
	ustack[1] = argc;
	ustack[2] = sp - (argc + 1) * 4; 
	sp -= (3 + argc + 1) * 4;
	copy_out(pgdir, sp, ustack, (3 + argc + 1) * 4);

   char *last = path;
   if(*path == '/') {
      last = path + 1;
   }
   safestrcpy(kcpu->current_task->name, last, sizeof(kcpu->current_task->name));

	page_dir_t *oldpgdir = kcpu->current_task->page_dir;
	kcpu->current_task->page_dir = pgdir;
	kcpu->current_task->size = sz;
	kcpu->current_task->tf->eip = elf.entry;
	kcpu->current_task->tf->esp = sp;
	switch_uvm(kcpu->current_task);
	free_user_page_dir(oldpgdir);
	return 0;
}
