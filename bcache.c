#include "system.h"
#include "spinlock.h"
#include "param.h"
#include "buf.h"

struct {
	spinlock_t lock;
	struct buf buf[NBUF];
	struct buf head;
} bcache;


void init_bcache() {
	struct buf *b;
	initlock(&bcache.lock, "bcache");
	
	bcache.head.prev = &bcache.head;
	bcache.head.next = &bcache.head;
	for(b = bcache.buf; b < bcache.buf+NBUF; b++) {
		b->next = bcache.head.next;
		b->prev = &bcache.head;
		b->dev = -1;
		bcache.head.next->prev = b;
		bcache.head.next = b;
	}
}

static struct buf *get_block(u32 dev, u32 blockno) {
	struct buf *b;
	acquire(&bcache.lock);
	loop:
	for(b = bcache.head.next; b != &bcache.head; b = b->next) {
		if(b->dev == dev && b->blockno == blockno) {
			if(!(b->flags & B_BUSY)) {
				b->flags |= B_BUSY;
				release(&bcache.lock);
				return b;
			}
			sleep(b, &bcache.lock);
			goto loop;
		}
	}

	for(b = bcache.head.prev; b != &bcache.head; b = b->prev) {
		if((b->flags & B_BUSY) == 0 && (b->flags & B_DIRTY) == 0) {
			b->dev = dev;
			b->blockno = blockno;
			b->flags = B_BUSY;
			release(&bcache.lock);
			return b;
		}
	}
	PANIC("get_block: no buffers");
	return 0;
}

struct buf *read_block(u32 dev, u32 blockno) {
	struct buf *b;
	b = get_block(dev, blockno);
	if(!(b->flags & B_VALID)) {
		rw_ide(b);
	}
	return b;
}

void write_block(struct buf *b) {
	if((b->flags & B_BUSY) == 0) {
		PANIC("write_block");
	}
	b->flags |= B_DIRTY;
	rw_ide(b);
}

void release_block(struct buf *b) {
	if((b->flags & B_BUSY) == 0) {
		PANIC("release_block");
	}
	acquire(&bcache.lock);
	b->next->prev = b->prev;
	b->prev->next = b->next;
	b->next = bcache.head.next;
	b->prev = &bcache.head;
	bcache.head.next->prev = b;
	bcache.head.next = b;
	b->flags &= ~B_BUSY;
	wakeup(b);
	release(&bcache.lock);
}

