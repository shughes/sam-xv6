#include "kheap.h"
#include "system.h"
#include "ordered_array.h"
#include "memlayout.h"
#include "monitor.h"
#include "vm.h"
#include "string.h"

char int_lessthan_pred(type_t a, type_t b) {
	int *x = a;
	int *y = b;
	return (*x < *y) ? 1 : 0;
}

char heap_lessthan_pred(type_t a, type_t b) {
	header_t *ha = (header_t *) a;
	header_t *hb = (header_t *) b;
	return (ha->size < hb->size) ? 1 : 0;
}


extern char end[];
extern page_dir_t *kernel_dir; 
u32 kaddress = (u32) V2P(end);
heap_t *kheap = 0;


static char* kalloc_int(u32 size, int align, u32 *phys) {
	if(kheap == 0) {
		if(align) {
			kaddress = PAGE_ROUND_UP(kaddress);
		}
		if(phys) {
			*phys = kaddress;
		}
		u32 addr = (u32) P2V(kaddress);
		kaddress += size;
		return (char *) addr;
	}
	else {
		void *addr = alloc(size, align, kheap);
		// TODO: See if I should add this:
		// memset(addr, 0, size);
		if(phys) {
			page_t *page = get_page(kernel_dir, (u32) addr, 0);
			*phys = (page->frame << 0xC) + ((u32) addr & 0xFFF);
		}
		// log_heap();
		return (char *) addr;
	}
}

char* kalloc_p(u32 size, u32 *phys) {
	return kalloc_int(size, 0, phys);
}

char* kalloc_a(u32 size) {
	u32 phys;
	return kalloc_int(size, 1, &phys);
}

char* kalloc_ap(u32 size, u32 *phys) {
	return kalloc_int(size, 1, phys);
}

char* kalloc(u32 size) {
	u32 phys;
	return kalloc_int(size, 0, &phys);
}


// assumes size includes header and footer
static int find_smallest_hole(u32 size, u8 page_align, heap_t *heap) {
	u32 i = 0;
	ordered_array_t index = heap->index;
	while(i < index.size) {
		header_t *header = (header_t *) lookup_ordered_array(i, &heap->index);
		if(page_align > 0) {
			u32 location = (u32) header;
			int offset = 0;
			if(((location + sizeof(header_t)) & 0xFFF) != 0) {
				offset = PAGE_ROUND_UP(location + sizeof(header_t)) - (location + sizeof(header_t));
			}
			if((header->size - offset) >= size) {
				break;
			}
		}
		else if(header->size >= size) {
			break;
		}
		i++;
	}
	if(i == index.size) {
		i = -1;
	}
	return i;
}

void free(void *p, heap_t *heap);

void expand(u32 new_size, heap_t *heap) {
	// monitor_write("expanding\n");
	ASSERT(new_size > (heap->end_address - heap->start_address));
	new_size = PAGE_ROUND_UP(new_size);
	// monitor_write_hex_ln(new_size);
	// monitor_write_hex_ln(heap->max_address - heap->start_address);
	ASSERT(new_size <= (heap->max_address - heap->start_address));
	u32 old_size = heap->end_address - heap->start_address;
	ASSERT((new_size - old_size) > ( sizeof(header_t) + sizeof(footer_t)));
	
	u32 i = old_size;
	while(i <= new_size) {
		alloc_frame(get_page(kernel_dir, heap->start_address + i, 1), 
			heap->supervisor, !heap->readonly);
		i += PGSIZE;
	}

	header_t *new_header = (header_t *) heap->end_address;
	new_header->magic = HEAP_MAGIC;
	new_header->size = new_size - old_size;
	new_header->is_hole = 1;
	footer_t *new_footer = (footer_t *) ((u32) new_header + new_header->size - sizeof(footer_t));
	new_footer->magic = HEAP_MAGIC;
	new_footer->header = new_header;
	
	heap->end_address = heap->start_address + new_size;

	free((void *) ((u32)new_header + sizeof(header_t)), heap);
}

heap_t *create_heap(u32 start, u32 end, u32 max, u8 supervisor, u8 readonly) {
	heap_t *heap = (heap_t *) kalloc(sizeof(heap_t));

	ASSERT(end % PGSIZE == 0);
	ASSERT(start % PGSIZE == 0);

	heap->index = place_ordered_array((void *) start, HEAP_INDEX_SIZE, heap_lessthan_pred);

	u32 start_address = start + HEAP_INDEX_SIZE * sizeof(type_t);
	
	ASSERT(start_address < end);

	heap->start_address = start_address;
	heap->end_address = end;
	heap->max_address = max;
	heap->supervisor = supervisor;
	heap->readonly = readonly;

	header_t *header = (header_t *) start_address;
	header->magic = HEAP_MAGIC;
	header->is_hole = 1;
	// size assumes header and footer included
	header->size = end - start_address;

	footer_t *footer = (footer_t *) ((u32) header + header->size - sizeof(footer_t));
	footer->magic = HEAP_MAGIC;
	footer->header = header;

	insert_ordered_array(header, &heap->index);

	return heap;
}


void log_heap() {
	heap_t *heap = kheap;
	ordered_array_t *array = &heap->index;
	int i;
	for(i = 0; i < array->size; i++) {
		header_t *header = (header_t *) lookup_ordered_array(i, array);
		monitor_write("index: ");
		monitor_write_hex(i);
		monitor_write(", address: ");
		monitor_write_hex((u32)header);
		monitor_write(", size: ");
		monitor_write_hex(header->size);
		monitor_write("\n");
	}
	monitor_write("\n");
}


static u8 place_hole(u32 size, u32 addr, heap_t *heap) {
	if(size > ( sizeof(header_t) + sizeof(footer_t))) {
		header_t *new_header = (header_t *) addr;
		new_header->size = size;
		new_header->is_hole = 1;
		new_header->magic = HEAP_MAGIC;
		footer_t *new_footer = (footer_t *) ((u32) new_header + new_header->size - sizeof(footer_t));
		new_footer->magic = HEAP_MAGIC;
		new_footer->header = new_header;
		insert_ordered_array(new_header, &heap->index);
		return 1;
	} else {
		return 0;
	}
}


void kfree(void *p) {
	// log_heap();
	free(p, kheap);
	// log_heap();
}

static void remove_combined(heap_t *heap, header_t *header) {
	int i;
	for(i = 0; i < heap->index.size; i++) {
		header_t *iter = (header_t *) lookup_ordered_array(i, &heap->index);
		if((u32) iter == (u32) header) {
			break;
		}
	}
	remove_ordered_array(i, &heap->index);
}

void free(void *p, heap_t *heap) {
	header_t *header = (header_t *) ((u32) p - sizeof(header_t));

	if(header->magic == HEAP_MAGIC && header->is_hole == 0) {
		header->is_hole = 1;
		footer_t *left_footer = (footer_t *) ((u32) header - sizeof(footer_t));
		if(left_footer->magic == HEAP_MAGIC) {
			header_t *left_header = left_footer->header;
			if(left_header->is_hole == 1) {
				left_header->size = left_header->size + header->size;
				footer_t *new_left_footer = 
					(footer_t *) ((u32) left_header + left_header->size - sizeof(footer_t));
				new_left_footer->header = left_header;
				header = left_header;
				remove_combined(heap, left_header);
			}
		}
		header_t *right_header = (header_t *) ((u32) header + header->size);
		if(right_header->magic == HEAP_MAGIC) {
			if(right_header->is_hole == 1) {
				header->size = header->size + right_header->size;
				footer_t *right_footer = (footer_t *) ((u32) header + header->size - sizeof(footer_t));
				right_footer->header = header;
				remove_combined(heap, right_header);
			}
		}
		insert_ordered_array(header, &heap->index);
	}
}

// size does not include header or footer
void *alloc(u32 size, u8 page_align, heap_t *heap) {
	u32 new_size = size + sizeof(header_t) + sizeof(footer_t);
	int i = find_smallest_hole(new_size, page_align, heap);
	if(i == -1) {
		u32 old_size = heap->end_address - heap->start_address;
		expand(old_size + new_size, heap);
		return alloc(size, page_align, heap);
	}
	header_t *header = (header_t *) lookup_ordered_array(i, &heap->index);
	remove_ordered_array(i, &heap->index);
	if(page_align > 0) {
		u32 old_header_addr = (u32) header;
		u32 s1 = header->size;
		footer_t *footer = (footer_t *)((u32) header + 
            (u32) header->size - (u32) sizeof(footer_t));

		header = (header_t *) PAGE_ROUND_UP((u32)header + sizeof(header_t));
		header = (header_t *) ((u32) header - sizeof(header_t));

		footer->header = header;
		u32 s2 = (u32) header - old_header_addr;
		header->size = s1 - s2;
		header->magic = HEAP_MAGIC;
		header->is_hole = 1;

		place_hole(s2, old_header_addr, heap);
	}
	u32 remaining_size = header->size - new_size;
	header->is_hole = 0;
	header_t *split_header = (header_t *) ((u32) header + new_size);
	u8 result = place_hole(remaining_size, (u32) split_header, heap);
	if(result == 1) {
		footer_t *footer = (footer_t *) ((u32) split_header - sizeof(footer_t));
		footer->header = header;
		footer->magic = HEAP_MAGIC;
		header->size = new_size;
	}
	return (void *) ((u32) header + sizeof(header_t));
}

