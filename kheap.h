#ifndef KHEAP_H
#define KHEAP_H

#include "system.h"
#include "types.h"
#include "ordered_array.h"


#define HEAP_INITIAL_SIZE 0x300000
#define HEAP_MAX_SIZE 0x400000
#define HEAP_INDEX_SIZE 0x20000
#define HEAP_MAGIC 0x123890AB


struct heap {
	ordered_array_t index;
	u32 start_address; // start of allocated space
	u32 end_address;	// end of allocated space
	u32 max_address; 	// max address it can expand to 
	u8 supervisor;
	u8 readonly;
};

typedef struct {
	u32 magic;
	u8 is_hole;
	u32 size;
} header_t;

typedef struct {
	u32 magic;
	header_t *header;
} footer_t;

heap_t *create_heap(u32 start, u32 end, u32 max, u8 supervisor, u8 readonly);
void *alloc(u32 size, u8 page_align, heap_t *heap);
void log_heap();
void expand(u32 new_size, heap_t *heap);

#endif // KHEAP_H
