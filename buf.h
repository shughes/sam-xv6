#ifndef BUF_H
#define BUF_H

#include "types.h"
#include "fs.h"

struct buf {
	int flags;
	u32 dev;
	u32 blockno;
	struct buf *prev;
	struct buf *next;
	struct buf *qnext;
	u8 data[BLOCK_SIZE];
};

#define B_BUSY  0x1
#define B_VALID 0x2
#define B_DIRTY 0x4 

#endif // BUF_H
