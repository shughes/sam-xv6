#ifndef VFS_H
#define VFS_H

#include "types.h"

#define FS_FILE        0x01
#define FS_DIRECTORY   0x02
#define FS_CHARDEVICE  0x03
#define FS_BLOCKDEVICE 0x04
#define FS_PIPE        0x05
#define FS_SYMLINK     0x06
#define FS_MOUNTPOINT  0x08 

typedef u32 (*read_type_t)(struct fs_node*, u32, u32, u8*);
typedef u32 (*write_type_t)(struct fs_node*, u32, u32, u8*);
typedef void (*open_type_t)(struct fs_node*);
typedef void (*close_type_t)(struct fs_node*);
typedef struct dir_entry *(*readdir_type_t)(struct fs_node*, u32);
typedef struct fs_node *(*finddir_type_t)(struct fs_node*, char* name);

struct dir_entry {
	char name[128];
	u32 ino;
};

typedef struct fs_node {
	char name[128];
	u32 mask;
	u32 uid;
	u32 gid;
	u32 flags;
	u32 length;
	read_type_t read;
	write_type_t write;
	open_type_t open;
	close_type_t close;
	readdir_type_t readdir;
	finddir_type_t finddir;
} fs_node_t;

#endif // VFS_H
