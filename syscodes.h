#ifndef SYSCODES_H
#define SYSCODES_H

#define SYS_write	0
#define SYS_exec 	1
#define SYS_fork 	2
#define SYS_exit 	3
#define SYS_open 	4
#define SYS_mknod 	5
#define SYS_dup 	6
#define SYS_close 	7
#define SYS_wait 	8
#define SYS_fstat 	9
#define SYS_read 	10
#define SYS_sbrk 	11
#define SYS_pipe 	12
#define SYS_chdir 	13

#endif
