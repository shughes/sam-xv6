#include "kheap.h"
// #include "system.h"
#include "ordered_array.h"
#include "memlayout.h"
#include "monitor.h"
#include "vm.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char standard_lessthan_pred(type_t a, type_t b) {
	return (a < b) ? 1 : 0;
}

/* ordered_array_t create_ordered_array(u32 max_size, lessthan_pred_t less_than) {
	ordered_array_t to_ret;
	to_ret.array = (type_t *) kalloc(sizeof(type_t) * max_size);
	memset((void *)to_ret.array, 0, (u32) sizeof(type_t) * max_size);
	to_ret.size = 0;
	to_ret.max_size = max_size;
	to_ret.less_than = less_than;
	return to_ret;
} */

ordered_array_t place_ordered_array(void *addr, u32 max_size, lessthan_pred_t less_than) {
	ordered_array_t to_ret;
	to_ret.array = (type_t *) addr;
	memset((void *) to_ret.array, 0, (u32) sizeof(type_t) * max_size);
	to_ret.max_size = max_size;
	to_ret.size = 0;
	to_ret.less_than = less_than;
	return to_ret;
}

void insert_ordered_array(type_t item, ordered_array_t *array) {
	int i = 0; 
	while (i < array->size && array->less_than(array->array[i], item)) {
		i++;
	}
	if(i == array->size) {
		array->array[array->size++] = item;
	}
	else {
		array->size++;
		while(i < array->size) {
			type_t old = array->array[i];
			array->array[i++] = item;
			item = old;
		}
	}
}

type_t lookup_ordered_array(u32 i, ordered_array_t *array) {
	return array->array[i];
}

void remove_ordered_array(u32 i, ordered_array_t *array) {
	while(i < array->size) {
		array->array[i] = array->array[i+1];
		i++;
	}
	array->size--;
}


char int_lessthan_pred(type_t a, type_t b) {
	int *x = a;
	int *y = b;
	return (*x < *y) ? 1 : 0;
}

char heap_lessthan_pred(type_t a, type_t b) {
	header_t *ha = (header_t *) a;
	header_t *hb = (header_t *) b;
	return (ha->size < hb->size) ? 1 : 0;
}


// extern char end[];
// extern page_dir_t *kernel_dir; 
// u32 kaddress = (u32) V2P(end);
heap_t *kheap = 0;


// assumes size includes header and footer
static int find_smallest_hole(u32 size, u8 page_align, heap_t *heap) {
	u32 i = 0;
	ordered_array_t index = heap->index;
	while(i < index.size) {
		header_t *header = (header_t *) lookup_ordered_array(i, &heap->index);
		if(page_align > 0) {
			u32 location = (u32) header;
			int offset = 0;
			if(((location + sizeof(header_t)) & 0xFFF) != 0) {
				offset = PAGE_ROUND_UP(location + sizeof(header_t)) - (location + sizeof(header_t));
			}
			if((header->size - offset) >= size) {
				break;
			}
		}
		else if(header->size >= size) {
			break;
		}
		i++;
	}
	if(i == index.size) {
		i = -1;
	}
	return i;
}

void panic_assert(char *file, u32 line, char *desc) {
	printf("ASSERTION FAILED(%s) at %s:%X\n", desc, file, line);
	for(;;);
}

#define ASSERT(msg) ((msg) ? (void) 0 : panic_assert(__FILE__, __LINE__, #msg))



static void remove_combined(heap_t *heap, header_t *header) {
	int i;
	for(i = 0; i < heap->index.size; i++) {
		header_t *iter = (header_t *) lookup_ordered_array(i, &heap->index);
		if((u32) iter == (u32) header) {
			break;
		}
	}
	remove_ordered_array(i, &heap->index);
}

void kfree(void *p, heap_t *heap) {
	header_t *header = (header_t *) ((u32) p - sizeof(header_t));

	if(header->magic == HEAP_MAGIC) {
		header->is_hole = 1;
		footer_t *left_footer = (footer_t *) ((u32) header - sizeof(footer_t));
		if(left_footer->magic == HEAP_MAGIC) {
			header_t *left_header = left_footer->header;
			if(left_header->is_hole == 1) {
				left_header->size = left_header->size + header->size;
				footer_t *new_left_footer = 
					(footer_t *) ((u32) left_header + left_header->size - sizeof(footer_t));
				new_left_footer->header = left_header;
				header = left_header;
				remove_combined(heap, left_header);
			}
		}
		header_t *right_header = (header_t *) ((u32) header + header->size);
		if(right_header->magic == HEAP_MAGIC) {
			if(right_header->is_hole == 1) {
				header->size = header->size + right_header->size;
				footer_t *right_footer = (footer_t *) ((u32) header + header->size - sizeof(footer_t));
				right_footer->header = header;
				remove_combined(heap, right_header);
			}
		}
		insert_ordered_array(header, &heap->index);
	}
}

void expand(u32 new_size, heap_t *heap) {
	// printf("expanding\n");
	ASSERT(new_size > (heap->end_address - heap->start_address));
	new_size = PAGE_ROUND_UP(new_size);
	// monitor_write_hex_ln(new_size);
	// monitor_write_hex_ln(heap->max_address - heap->start_address);
	ASSERT(new_size <= (heap->max_address - heap->start_address));
	u32 old_size = heap->end_address - heap->start_address;
	ASSERT((new_size - old_size) > ( sizeof(header_t) + sizeof(footer_t)));
	
	u32 i = old_size;
	while(i <= new_size) {
		// alloc_frame(get_page(kernel_dir, heap->start_address + i, 1), 
			// heap->supervisor, !heap->readonly);
		i += PGSIZE;
	}

	header_t *new_header = (header_t *) heap->end_address;
	new_header->magic = HEAP_MAGIC;
	new_header->size = new_size - old_size;
	new_header->is_hole = 1;
	footer_t *new_footer = (footer_t *) ((u32) new_header + new_header->size - sizeof(footer_t));
	new_footer->magic = HEAP_MAGIC;
	new_footer->header = new_header;
	
	heap->end_address = heap->start_address + new_size;

	kfree((void *) ((u32)new_header + sizeof(header_t)), heap);
}

char *kbuf;

heap_t *create_heap(u32 start, u32 end, u32 max, u8 supervisor, u8 readonly) {
	heap_t *heap = (heap_t *) malloc(sizeof(heap_t));

	ASSERT(end % PGSIZE == 0);
	ASSERT(start % PGSIZE == 0);

	heap->index = place_ordered_array((void *) start, HEAP_INDEX_SIZE, heap_lessthan_pred);

	u32 start_address = start + HEAP_INDEX_SIZE * sizeof(type_t);
	
	ASSERT(start_address < end);

	heap->start_address = start_address;
	heap->end_address = end;
	heap->max_address = max;
	heap->supervisor = supervisor;
	heap->readonly = readonly;

	header_t *header = (header_t *) start_address;
	header->magic = HEAP_MAGIC;
	header->is_hole = 1;
	// size assumes header and footer included
	header->size = end - start_address;

	footer_t *footer = (footer_t *) ((u32) header + header->size - sizeof(footer_t));
	footer->magic = HEAP_MAGIC;
	footer->header = header;

	insert_ordered_array(header, &heap->index);

	return heap;
}


void log_heap() {
	heap_t *heap = kheap;
	ordered_array_t *array = &heap->index;
	int i;
	for(i = 0; i < array->size; i++) {
		header_t *header = (header_t *) lookup_ordered_array(i, array);
		printf("index: %X", i);
		printf(", address: %X", (u32)header);
		printf(", size: %X", header->size);
		printf("\n");
	}
	printf("\n");
}


static u8 place_hole(u32 size, u32 addr, heap_t *heap) {
	if(size > ( sizeof(header_t) + sizeof(footer_t))) {
		header_t *new_header = (header_t *) addr;
		new_header->size = size;
		new_header->is_hole = 1;
		new_header->magic = HEAP_MAGIC;
		footer_t *new_footer = (footer_t *) ((u32) new_header + new_header->size - sizeof(footer_t));
		new_footer->magic = HEAP_MAGIC;
		new_footer->header = new_header;
		insert_ordered_array(new_header, &heap->index);
		return 1;
	} else {
		return 0;
	}
}

// size does not include header or footer
void *alloc(u32 size, u8 page_align, heap_t *heap) {
	u32 new_size = size + sizeof(header_t) + sizeof(footer_t);
	int i = find_smallest_hole(new_size, page_align, heap);
	if(i == -1) {
		u32 old_size = heap->end_address - heap->start_address;
		expand(old_size + new_size, heap);
		return alloc(size, page_align, heap);
	}
	header_t *header = (header_t *) lookup_ordered_array(i, &heap->index);
	remove_ordered_array(i, &heap->index);
	if(page_align > 0) {
		u32 old_header_addr = (u32) header;
		u32 s1 = header->size;
		footer_t *footer = (footer_t *)((u32) header + (u32) header->size - (u32) sizeof(footer_t));

		header = (header_t *) PAGE_ROUND_UP((u32)header + sizeof(header_t));
		header = (header_t *) ((u32) header - sizeof(header_t));

		footer->header = header;
		u32 s2 = (u32) header - old_header_addr;
		header->size = s1 - s2;
		header->magic = HEAP_MAGIC;
		header->is_hole = 1;
		place_hole(s2, old_header_addr, heap);
	}
	u32 remaining_size = header->size - new_size;
	header->is_hole = 0;
	header_t *split_header = (header_t *) ((u32) header + new_size);
	u8 result = place_hole(remaining_size, (u32) split_header, heap);
	if(result == 1) {
		footer_t *footer = (footer_t *) ((u32) split_header - sizeof(footer_t));
		footer->header = header;
		footer->magic = HEAP_MAGIC;
		header->size = new_size;
	}
	return (void *) ((u32) header + sizeof(header_t));
}

#define KERNEL_STACK_SIZE 2048

int main() {
	kbuf = (char *) malloc(0x400000);
	kheap = create_heap((u32)kbuf+0x1000, (u32)kbuf+0x100000, (u32)kbuf+0x200000, 0, 0);
	log_heap();
	void *buf1 = alloc(KERNEL_STACK_SIZE, 1, kheap);
	void *buf2 = alloc(sizeof(page_dir_t), 1, kheap);
	void *buf3 = alloc(sizeof(page_table_t), 1, kheap);
	log_heap();
	kfree(buf1, kheap);
	void *buf4 = alloc(KERNEL_STACK_SIZE, 1, kheap);
	kfree(buf2, kheap);
	kfree(buf3, kheap);
	log_heap();
	return 0;
}

