
[GLOBAL swtch]
swtch:
	mov eax, [esp+4]			; & source context pointer
	mov edx, [esp+8]			; destination context pointer

	push ebp
	push ebx
	push esi
	push edi

	;; switch stacks
	mov [eax], esp
	mov esp, edx
	
	pop edi
	pop esi
	pop ebx
	pop ebp
	ret


	
[GLOBAL trapret]
trapret:
	popa
	pop gs
	pop fs
	pop es
	pop ds
	add esp, 0x8
	iret
	
extern isr_handler

isr_common_stub:
	;; push trapframe
	push ds
	push es
	push fs
	push gs
	pusha

	;; switch to kernel mode
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	push esp					; trapframe ptr
	call isr_handler
	add esp, 4
	
	jmp trapret


extern irq_handler
	
irq_common_stub:
	;; trapframe
	push ds
	push es
	push fs
	push gs
	pusha

	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	
	push esp					; trapframe ptr
	call irq_handler
	add esp, 4
	
	jmp trapret



%macro ISR 1	
	[GLOBAL isr%1]		
	isr%1:
		push byte 0
		push byte %1
		jmp isr_common_stub
%endmacro

%macro ISR_ERR 1
	[GLOBAL isr%1]
	isr%1:
		push byte %1
		jmp isr_common_stub
%endmacro

%macro IRQ 2
	global irq%1
	irq%1:
		push byte 0
		push byte %2
		jmp irq_common_stub
%endmacro

ISR 0
ISR 1
ISR 2
ISR 3
ISR 4
ISR 5
ISR 6
ISR 7
ISR_ERR   8
ISR 9
ISR_ERR   10
ISR_ERR   11
ISR_ERR   12
ISR_ERR   13
ISR_ERR   14
ISR 15
ISR 16
ISR 17
ISR 18
ISR 19
ISR 20
ISR 21
ISR 22
ISR 23
ISR 24
ISR 25
ISR 26
ISR 27
ISR 28
ISR 29
ISR 30
ISR 31

ISR 128

IRQ   0,    32
IRQ   1,    33
IRQ   2,    34
IRQ   3,    35
IRQ   4,    36
IRQ   5,    37
IRQ   6,    38
IRQ   7,    39
IRQ   8,    40
IRQ   9,    41
IRQ  10,    42
IRQ  11,    43
IRQ  12,    44
IRQ  13,    45
IRQ  14,    46
IRQ  15,    47
