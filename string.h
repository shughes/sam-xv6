#ifndef STRING_H
#define STRING_H

#include "types.h"

void* memset(void *dst, int c, u32 n);
void* memcpy(void *dst, const void *src, u32 n);
void* memmove(void *dst, const void *src, u32 n);
int strncmp(const char *p, const char *q, u32 n);
char* strncpy(char *s, const char *t, int n);
int strlen(const char *s);
char* safestrcpy(char *s, const char *t, int n);

#endif // STRING_H
