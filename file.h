#ifndef FILE_H
#define FILE_H 

#include "fs.h"
#include "types.h"

typedef struct inode {
	u32 dev;
	u32 inum;
	int ref;
	int flags;

	s16 type;
	s16 major;
	s16 minor;
	s16 nlink;

	u32 size;
	u32 addrs[DIRECT_SIZE+1];
} inode_t;

typedef struct file {
	enum { FD_NODE, FD_PIPE, FD_INODE } type;
	int ref;
	char readable;
	char writeable;
	inode_t *ip;
	u32 off;
} file_t;

typedef struct devsw {
	int (*read)(inode_t*, char*, int);
	int (*write)(inode_t*, char*, int);
} devsw_t;

extern devsw_t kdevsw[];

#define I_BUSY 0x1
#define I_VALID 0x2

#define CONSOLE 1

#endif // FILE_H
