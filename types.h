#ifndef TYPES_H
#define TYPES_H

typedef unsigned int   u32;
typedef int s32;
typedef short s16;
typedef unsigned short u16;
typedef unsigned char  u8;
typedef unsigned long  ulong;
typedef u32 pde_t;

#endif 
