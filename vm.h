#ifndef VM_H
#define VM_H

#include "system.h"
#include "mmu.h"


#define PAGE_ROUND_DOWN(a) ((a)) & ~(PGSIZE-1)
#define PAGE_ROUND_UP(a) (((a)+PGSIZE-1) & ~(PGSIZE-1)) 

struct page {
	u32 present    : 1;   // Page present in memory
	u32 rw         : 1;   // Read-only if clear, readwrite if set
	u32 user       : 1;   // Supervisor level only if clear
	u32 accessed   : 1;   // Has the page been accessed since last refresh?
	u32 dirty      : 1;   // Has the page been written to since last refresh?
	u32 unused     : 7;   // Amalgamation of unused and reserved bits
	u32 frame      : 20;  // Frame address (shifted right 12 bits)
};

struct page_table {
	struct page pages[NPTENTRIES];
};


struct page_dir {
	struct page_table* tables[NPDENTRIES];
	u32 phys_addresses[NPDENTRIES];
	u32 address;
};


#endif // VM_H
