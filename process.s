

[GLOBAL read_eip]
read_eip:
	pop eax
	jmp eax


[GLOBAL copy_full_page]
copy_full_page:
	push ebp
	mov ebp, esp

	mov ecx, [ebp+24] 	; stack buffer

	mov eax, [ebp+8]
	mov [ecx], eax		; current page dir

	mov eax, [ebp+20]
	mov [ecx+4], eax	; kernel buffer

	mov eax, [ebp+28]
	mov [ecx+8], eax	; page to copy

	mov eax, [ebp+16]
	mov [ecx+12], eax	; destination page dir

	mov eax, [ebp+20]
	mov [ecx+16], eax	; original kernel buffer start

	mov eax, [ebp+28]	
	mov [ecx+20], eax	; original page to copy start

	mov eax, [ebp+12]	; source page dir
	mov cr3, eax

	mov edx, 1024
.loop:
	mov eax, [ecx+8]
	mov ebx, [eax]
	mov eax, [ecx+4]
	mov [eax], ebx		; copy from page to kernel buffer

	add eax, 4
	mov [ecx+4], eax	; increase kernel buffer pointer

	mov eax, [ecx+8]
	add eax, 4
	mov [ecx+8], eax	; increase page to copy pointer

	dec edx
	jnz .loop

	mov eax, [ecx+12]
	mov cr3, eax		; switching to destination

	mov edx, 1024
.loop2:
	mov eax, [ecx+16]
	mov ebx, [eax]
	mov eax, [ecx+20]
	mov [eax], ebx

	add eax, 4
	mov [ecx+20], eax

	mov eax, [ecx+16]
	add eax, 4
	mov [ecx+16], eax

	dec edx
	jnz .loop2

	mov eax, [ecx]
	mov cr3, eax

	leave
	ret



[GLOBAL do_switch]
do_switch:
	pop eax 		; return addr
	pop ecx 		; passed in eip
	pop eax			; passed in esp
	pop ebx			; passed in ebp
	pop edx			; passed in cr3
	mov esp, eax
	mov ebp, ebx
	mov cr3, edx
	mov eax, 0x12345
	jmp ecx


	
