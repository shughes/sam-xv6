#include "task.h"
#include "system.h"
#include "string.h"
#include "monitor.h"
#include "mmu.h"
#include "x86.h"
#include "spinlock.h"

cpu_t *kcpu;

extern page_dir_t *kpgdir;

static void move_stack(void *new_address, u32 size, u32 stack_start) {
   u32 i = (u32) new_address;
   for(i = (u32) new_address; i >= ((u32)new_address - size); i -= PGSIZE) {
      alloc_frame(get_page(kpgdir, i, 1), 0, 1);
   }

   u32 pd_addr;
   asm volatile("mov %%cr3, %0" : "=r" (pd_addr));
   asm volatile("mov %0, %%cr3" ::"r" (pd_addr));

   u32 old_stack_pointer;
   asm volatile("mov %%esp, %0" : "=r" (old_stack_pointer));
   u32 old_base_pointer;
   asm volatile("mov %%ebp, %0" : "=r" (old_base_pointer));

   u32 offset = stack_start - old_stack_pointer;
   u32 new_stack_pointer = (u32) new_address - offset;
   memcpy((void *)new_stack_pointer, (void *) old_stack_pointer, offset);
   u32 stack_offset = new_stack_pointer - old_stack_pointer;
   u32 new_base_pointer = old_base_pointer + stack_offset;

   for(i = new_stack_pointer; i < (u32) new_address; i += 4) {
      u32 *val = (u32 *) i;
      if(*val >= old_stack_pointer && *val < stack_start) {
         *val = *val + stack_offset;
      }
   }

   asm volatile("mov %0, %%esp" : : "r" (new_stack_pointer));
   asm volatile("mov %0, %%ebp" : : "r" (new_base_pointer));
}

extern page_dir_t *kpgdir;
extern char kstack_start[];


int getpid() {
   return kcpu->current_task->id;
}

static void enqueue_task(task_t* new_task, task_t* queue) {
   while(queue->next) {
      queue = queue->next;
   }
   queue->next = new_task;
   new_task->next = 0;
}

void forkret() {
   release(&kcpu->lock);
}

extern void trapret();

static task_t *alloc_task() {
   task_t *new_task = (task_t *) kalloc(sizeof(task_t));
   new_task->state = UNUSED;
   new_task->id = kcpu->next_pid++;
   new_task->kernel_stack = (u32) kalloc_a(KERNEL_STACK_SIZE);
   char *sp = (char *) new_task->kernel_stack + KERNEL_STACK_SIZE;
   sp -= sizeof(*new_task->tf);
   new_task->tf = (trapframe_t *) sp;
   sp -= 4;
   *(u32 *)sp = (u32) trapret;
   sp -= sizeof(*new_task->context);
   new_task->context = (context_t *) sp;
   memset(new_task->context, 0, sizeof(*new_task->context));
   new_task->context->eip = (u32) forkret;
   return new_task;
}

int grow_task(int n) {
   u32 size = kcpu->current_task->size;
   u32 new_size;
   if(n > 0) {
      new_size = alloc_uvm(kcpu->current_task->page_dir, size, size + n);
   } else if(n < 0) {
      new_size = dealloc_uvm(kcpu->current_task->page_dir, size, size + n);
   }
   kcpu->current_task->size = new_size;
   switch_uvm(kcpu->current_task);
   return 0;
}

void init_tasking() {
   kcpu = (cpu_t *) kalloc(sizeof(cpu_t));
   kcpu->ncli = 0;
   kcpu->next_pid = 1;
   initlock(&kcpu->lock, "ptable");

   pushcli();
   move_stack((void *)0xE0000000, 0x2000, (u32)kstack_start);
   task_t *t = alloc_task();
   t->page_dir = clone_directory(kpgdir);
   init_uvm(t->page_dir);
   t->next = 0;
   t->state = RUNNABLE;
   t->tf->cs = 0x1B;
   t->tf->ds = 0x23;
   t->tf->es = 0x23;
   t->tf->ss = 0x23;
   t->tf->eflags = 0x200;
   t->tf->esp = PGSIZE;
   t->size = PGSIZE;
   t->tf->eip = 0;
   t->cwd = lookup_inode("/");
   kcpu->current_task = t;
   kcpu->task_table = t;
   kcpu->init_task = t;
   popcli();
}

int fork() {
   task_t *parent_task = (task_t *) kcpu->current_task;
   task_t *new_task = alloc_task();
   new_task->page_dir = clone_directory(kpgdir);
   new_task->parent = parent_task;

   new_task->size = parent_task->size;

   *new_task->tf = *parent_task->tf;
   new_task->tf->eax = 0;

   int i;
   for(i = 0; i < NOFILE; i++) {
      if(kcpu->current_task->ofile[i]) {
         new_task->ofile[i] = inc_file_ref(kcpu->current_task->ofile[i]);
      }
   }
   new_task->cwd = inc_inode_ref(parent_task->cwd);

   acquire(&kcpu->lock);
   enqueue_task(new_task, (task_t *)kcpu->task_table);
   new_task->state = RUNNABLE;
   release(&kcpu->lock);

   return new_task->id;
}

void kfree(void *p);

int wait() {
   task_t *p;
   int havekids, pid;

   acquire(&kcpu->lock);
   for(;;){
      // Scan through table looking for zombie children.
      havekids = 0;
      for(p = kcpu->task_table; p != 0; p = p->next){
         if(p->parent != kcpu->current_task) {
            continue;
         }
         havekids = 1;
         if(p->state == ZOMBIE) {
            // Found one.
            pid = p->id;
            kfree((void *)p->kernel_stack);
            p->kernel_stack = 0;
            free_user_page_dir(p->page_dir);
            p->state = UNUSED;
            p->id = 0;
            p->parent = 0;
            // p->name[0] = 0;
            p->killed = 0;
            release(&kcpu->lock);
            return pid;
         }
      }

      // No point waiting if we don't have any children.
      if(!havekids || kcpu->current_task->killed){
         release(&kcpu->lock);
         return -1;
      }

      // Wait for children to exit.  (See wakeup1 call in proc_exit.)
      sleep(kcpu->current_task, &kcpu->lock);  //DOC: wait-sleep
   }
}

static void wakeup1(void *chan);


void exit() {
   int fd;
   for(fd = 0; fd < NOFILE; fd++) {
      if(kcpu->current_task->ofile[fd]) {
         dec_file_ref(kcpu->current_task->ofile[fd]);
         kcpu->current_task->ofile[fd] = 0;
      }
   }

   // begin_op();
   dec_inode_ref(kcpu->current_task->cwd);
   // end_op();
   kcpu->current_task->cwd = 0;

   acquire(&kcpu->lock);

   wakeup1(kcpu->current_task->parent);

   // pass abandoned children to init_task. 
   task_t *t;
   for(t = kcpu->task_table; t != 0; t = t->next) {
      if(t->parent == kcpu->current_task) {
         t->parent = kcpu->init_task;
         if(t->state == ZOMBIE) {
            wakeup1(kcpu->init_task);
         }
      }
   }

   kcpu->current_task->state = ZOMBIE;
   sched();
   PANIC("ZOMBIE exit");
}

extern void swtch(context_t **, context_t *);

void scheduler() {
   task_t *t;
   for(;;) {
      sti();
      acquire(&kcpu->lock);
      for(t = kcpu->task_table; t != 0; t = t->next) {
         if(t->state == RUNNABLE) {
            kcpu->current_task = t;
            switch_uvm(t);
            t->state = RUNNING;
            swtch(&kcpu->sched_context, t->context);
            switch_kvm();
            kcpu->current_task = 0;
         }
      }
      release(&kcpu->lock);
   }
}

void sched() {
   swtch(&kcpu->current_task->context, kcpu->sched_context);
}

void yield() {
   acquire(&kcpu->lock);
   kcpu->current_task->state = RUNNABLE;
   sched();
   release(&kcpu->lock);
}

void sleep(void *chan, spinlock_t *lock) {
   if(kcpu->current_task == 0) {
      PANIC("sleep()");
   }
   if(lock == 0) { 
      PANIC("sleep without lock");
   }
   if(lock != &kcpu->lock) {
      acquire(&kcpu->lock);
      release(lock);
   }
   kcpu->current_task->chan = chan;
   kcpu->current_task->state = SLEEPING;
   sched();
   kcpu->current_task->chan = 0;
   if(lock != &kcpu->lock) {
      release(&kcpu->lock);
      acquire(lock);
   }
}

static void wakeup1(void *chan) {
   task_t *t;
   for(t = kcpu->task_table; t != 0; t = t->next) {
      if(t->state == SLEEPING && t->chan == chan) {
         t->state = RUNNABLE;
      }
   }
}

void wakeup(void *chan) {
   acquire(&kcpu->lock);
   wakeup1(chan);
   release(&kcpu->lock);
}
