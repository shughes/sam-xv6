#include "isr.h"
#include "system.h"
#include "x86.h"

isr_t kinterrupt_handlers[256];

void isr_handler(trapframe_t *tf) {
	u8 int_no = tf->trapno & 0xFF;
	if(kinterrupt_handlers[int_no] != 0) {
		isr_t handler = kinterrupt_handlers[int_no];
		handler(tf);
	}
	else {
		monitor_write("Received ISR interrupt: ");
		monitor_write_hex(tf->trapno);
		monitor_write("\n");
	}
}

void irq_handler(trapframe_t *tf) {
	if(tf->trapno >= 40) {
		outb(0xA0, 0x20);
	}
	outb(0x20, 0x20);
	if(kinterrupt_handlers[tf->trapno] != 0) {
		isr_t handler = kinterrupt_handlers[tf->trapno];
		handler(tf);
	}
	else {
		/*monitor_write("Received IRQ interrupt: ");
		monitor_write_hex(regs.int_no);
		monitor_write("\n");*/
	}
}

void register_interrupt_handler(u8 n, isr_t handler) {
	kinterrupt_handlers[n] = handler;
}
