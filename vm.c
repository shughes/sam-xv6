#include "vm.h"
#include "system.h"
#include "kheap.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "string.h"
#include "task.h"

static u32 kframe_map[0x700];

static u32 get_table_index(u32 addr) {
	return addr >> 0x16;
}

static u32 get_page_index(u32 addr) {
	return (addr & 0x3FFFFF) >> 0xC;
}

static void use_frame(u32 addr) {
	u32 frame_index = addr / PGSIZE;
	int map_index = frame_index / 32;
	int node_index = frame_index % 32;
	u32 *node = &kframe_map[map_index];
	*node = *node + (1 << node_index);
}

void free_frame(u32 addr) {
	u32 frame_index = addr / PGSIZE;
	int map_index = frame_index / 32;
	int node_index = frame_index % 32;
	u32 *node = &kframe_map[map_index];
	*node = *node & ~(1 << node_index);
}

static u32 get_free_frame() {
	u32 frame_node;
	int i;
	for(i = 0; i < SIZE(kframe_map); i++) {
		frame_node = kframe_map[i];
		if(frame_node != 0xFFFFFFFF) {
			int j;
			for(j = 0; j < 32; j++) {
				int val = (frame_node >> j) & 1;
				if(val == 0) {
					// found
					return i * 32 + j;
				}
			}
		}
	}
	return 0;
}

extern heap_t *kheap;

void alloc_frame(page_t* page, int is_kernel, int writeable) {
	if(page->frame != 0) {
		return;
	}
	u32 frame_index = get_free_frame();
	use_frame(frame_index * PGSIZE);
	page->present = 1;
	page->rw = (writeable) ? 1 : 0;
	page->user = (is_kernel) ? 0 : 1;
	page->frame = frame_index;
}


page_t *get_page(page_dir_t* pgdir, u32 address, int make) {
	u32 table_index = get_table_index(address);
	u32 page_index = get_page_index(address);
	page_table_t *table = pgdir->tables[table_index];
	if(table) {
		return &table->pages[page_index];
	} else if(make) {
		u32 addr;
		table = (page_table_t *) kalloc_ap(PGSIZE, &addr);
		pgdir->tables[table_index] = table;
		pgdir->phys_addresses[table_index] = addr | PTE_P | PTE_W | PTE_U;
		return &table->pages[page_index];
	} else {
		return 0;
	}
}

static void page_fault(trapframe_t *tf) {
	u32 faulting_address;
	asm volatile("mov %%cr2, %0" : "=r" (faulting_address));
	int present = !(tf->err & 0x1);
	int rw = tf->err & 0x2;
	int us = tf->err & 0x4;
	int reserved = tf->err & 0x8;
	monitor_write("Page fault (");
	if (present) {monitor_write("present ");}
	if (rw) {monitor_write("read-only ");}
	if (us) {monitor_write("user-mode ");}
	if (reserved) {monitor_write("reserved ");}
	monitor_write(") at ");
	monitor_write_hex(faulting_address);
	monitor_write("\n");
	PANIC("Page fault");
}

// __attribute__((__aligned__(PGSIZE)))
page_dir_t *kpgdir; 
page_dir_t *kernel_dir;

void switch_page_directory(page_dir_t *dir) {
	kpgdir = dir;
	asm volatile("mov %0, %%cr3" :: "r" (dir->address));

	u32 cr4;
	asm volatile("mov %%cr4, %0" : "=r" (cr4));
	cr4 &= ~CR4_PSE;
	asm volatile("mov %0, %%cr4" :: "r" (cr4));

	u32 cr0;
	asm volatile("mov %%cr0, %0" : "=r" (cr0));
	cr0 |= CR0_PG;
	asm volatile("mov %0, %%cr0" :: "r" (cr0));
}

extern u32 kaddress;

void init_vm() {
	kaddress = PAGE_ROUND_UP(kaddress);
	u32 phys;
	kernel_dir = (page_dir_t *) kalloc_ap(sizeof(page_dir_t), &phys);
	kernel_dir->address = phys + PGSIZE;

	u32 i = KERNBASE;
	u32 kvaddress = (u32) P2V(kaddress);
	while(i <= (kvaddress + 0x1000 + HEAP_INITIAL_SIZE)) {
		alloc_frame(get_page(kernel_dir, i, 1), 1, 1);
		i += PGSIZE;
		kvaddress = (u32) P2V(kaddress);
	}

	u32 heap_start = PAGE_ROUND_UP(kvaddress + 0x1000);
	register_interrupt_handler(14, page_fault);
	switch_page_directory(kernel_dir);

	kheap = create_heap(heap_start, heap_start + HEAP_INITIAL_SIZE, 
		heap_start + HEAP_MAX_SIZE, 0, 0);
}

void switch_uvm(task_t *t) {
	pushcli();
	set_kernel_stack(t->kernel_stack + KERNEL_STACK_SIZE);
	switch_page_directory(t->page_dir);
	popcli();
}

void switch_kvm() {
	switch_page_directory(kernel_dir);
}


u32 alloc_uvm(page_dir_t *pgdir, u32 old_size, u32 new_size) {
	u32 i = PAGE_ROUND_UP(old_size);
	while(i < new_size) {
		alloc_frame(get_page(pgdir, i, 1), 0, 1);
		pushcli();
		page_dir_t *cur_page = kpgdir;
		switch_page_directory(pgdir);
		memset((void *)i, 0, PGSIZE);
		switch_page_directory(cur_page);
		popcli();
		i += PGSIZE;
	}
	return new_size;
}

void init_uvm(page_dir_t* pgdir) {
	extern char _binary_initcode_start[], _binary_initcode_size[];
	alloc_frame(get_page(pgdir, 0, 1), 0, 1);
	page_dir_t *cur = kpgdir;
	switch_page_directory(pgdir);
	memmove(0, _binary_initcode_start, (int) _binary_initcode_size);
	switch_page_directory(cur);
}

u32 kpage_buffer[0x400];
u32 kstack_buffer[0x20];
extern char kstack_start[];

void copy_full_page(u32, u32, u32, u32, u32, u32);

static page_table_t *clone_table(u32 dir_i, const page_dir_t *dir_src, page_dir_t *dir_dest) {

	u32 phys_addr;

	page_table_t *src = dir_src->tables[dir_i];

	page_table_t *dest = (page_table_t *) kalloc_ap(sizeof(page_table_t), &phys_addr);
	memset(dest, 0, sizeof(page_table_t));
	int i;
	for(i = 0; i < 0x400; i++) {
		if(src->pages[i].frame == 0) {
			continue;
		}
		alloc_frame(&dest->pages[i], 0, 0);
		dest->pages[i].present = src->pages[i].present;
		dest->pages[i].rw = src->pages[i].rw;
		dest->pages[i].user = src->pages[i].user;
		dest->pages[i].accessed = src->pages[i].accessed;
		dest->pages[i].dirty = src->pages[i].dirty;
		dest->pages[i].unused = src->pages[i].unused;

		dir_dest->tables[dir_i] = dest;
		dir_dest->phys_addresses[dir_i] = phys_addr | 0x7;

		u32 addr = (dir_i << 0x16) + (i << 0xC);

		copy_full_page(kpgdir->address, dir_src->address, dir_dest->address, 
			(u32) kpage_buffer, (u32) kstack_buffer, addr);
	}
	return dest;
}

page_dir_t *clone_kernel_directory() {
	u32 phys;
	page_dir_t *dest = (page_dir_t *) kalloc_ap(sizeof(page_dir_t), &phys);
	dest->address = phys + PGSIZE;
	int i;
	for(i = 0; i < NPDENTRIES; i++) {
		if(kernel_dir->phys_addresses[i] == 0) {
			continue;
		}
		dest->phys_addresses[i] = kernel_dir->phys_addresses[i];
		dest->tables[i] = kernel_dir->tables[i];
	}
	return dest;
}

page_dir_t *clone_directory(page_dir_t *src) {
	page_dir_t *dest = clone_kernel_directory();
	int i;
	for(i = 0; i < NPDENTRIES; i++) {
		if(src->phys_addresses[i] == 0) {
			continue;
		}
		if(src->tables[i] != kernel_dir->tables[i]) {
			pushcli();
			clone_table(i, src, dest);
			popcli();
		}
	}
	return dest;
}

int dealloc_uvm(page_dir_t *pgdir, u32 old_size, u32 new_size) {
	u32 a = PAGE_ROUND_UP(new_size);
	while(a < old_size) {
		u32 ti = get_table_index(a);
		u32 pi = get_page_index(a);
		a += PGSIZE;
		if(pgdir->phys_addresses[ti] == 0) {
			continue;
		}
		if(pgdir->tables[ti] != kernel_dir->tables[ti]) {
			page_table_t *table = pgdir->tables[ti];
			page_t page = table->pages[pi];
			u32 frame = (page.frame << 0xC) & ~0xFFF;
			if(frame != 0) {
				free_frame(frame);
			}
		}
	}
	return new_size;
}

void kfree(void *p);

void free_user_page_dir(page_dir_t *pgdir) {
	dealloc_uvm(pgdir, KERNBASE, 0);
	u32 i;
	for(i = 0; i < NPDENTRIES; i++) {
		if(pgdir->phys_addresses[i] == 0) {
			continue;
		}
		if(pgdir->tables[i] != kernel_dir->tables[i]) {
			kfree((void *)pgdir->tables[i]);
		}
	}
	kfree((void *)pgdir);
}

void clear_page(page_dir_t *pgdir, char *vaddr) {
	u32 table_index = get_table_index((u32) vaddr);
	u32 page_index = get_page_index((u32) vaddr);
	page_table_t *table = pgdir->tables[table_index];
	page_t page = table->pages[page_index];
	page.user = 0;
	table->pages[page_index] = page;
}

char ktemp_pa[PGSIZE];

int load_uvm(page_dir_t *pgdir, char *addr, struct inode *ip, u32 offset, u32 sz) {
	u32 i, n;
	for(i = 0; i < sz; i += PGSIZE) {
		if((sz - i) < PGSIZE) {
			n = sz - i;
		}
		else {
			n = PGSIZE;
		}
		u32 pa = (u32) addr + i;
		read_inode(ip, (char *) ktemp_pa, offset + i, n);

		pushcli();
		page_dir_t *cur_page = kpgdir;
		switch_page_directory(pgdir);
		memmove((char *)pa, ktemp_pa, n);
		switch_page_directory(cur_page);
		popcli();
	}
	return 0;
}

int copy_out(page_dir_t *pgdir, u32 va, void *ptr, u32 length) {
	u32 i, n;
	for(i = 0; i < length; i += PGSIZE) {
		if((length - i) < PGSIZE) {
			n = length - i;
		}
		else {
			n = PGSIZE;
		}
		u32 pa = (u32) ptr + i;
		memmove(ktemp_pa, (void *) pa, n);
		pushcli();
		page_dir_t *cur_page = kpgdir;
		switch_page_directory(pgdir);
		memmove((char *)va, ktemp_pa, n);
		switch_page_directory(cur_page);
		popcli();
	}
	return 0;
}




