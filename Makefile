OBJS = \
	main.o\
	vm.o\
	gdt.o\
	interrupt.o\
	monitor.o\
	descriptor_tables.o\
	isr.o\
	string.o\
	timer.o\
	spinlock.o\
	task.o\
	ordered_array.o\
	kheap.o\
	process.o\
	file.o\
	syscall.o\
	exec.o\
	kb.o\
	ide.o\
	bcache.o\
	fs.o\


TOOLPREFIX = i386-jos-elf-
CC = $(TOOLPREFIX)gcc
LD = $(TOOLPREFIX)ld
OBJCOPY = $(TOOLPREFIX)objcopy
OBJDUMP = $(TOOLPREFIX)objdump
LDFLAGS += -m $(shell $(LD) -V | grep elf_i386 2>/dev/null)
CFLAGS = -fno-pic -static -fno-builtin -fno-strict-aliasing -fvar-tracking \
	-fvar-tracking-assignments -O0 -g -Wall -MD -gdwarf-2 -m32 -Werror -fno-omit-frame-pointer
CFLAGS += $(shell $(CC) -fno-stack-protector -E -x c /dev/null >/dev/null 2>&1 && echo \
	-fno-stack-protector)

default: xv6.img

test: 
	gcc -m32 heap_test.c -c 
	gcc -m32 -o a.out heap_test.o
	./a.out

.s.o: 
	nasm -felf $< 

bootblock: bootasm.S bootmain.c
	$(CC) $(CFLAGS) -fno-pic -O -nostdinc -I. -c bootmain.c
	$(CC) $(CFLAGS) -fno-pic -nostdinc -I. -c bootasm.S
	$(LD) $(LDFLAGS) -N -e start -Ttext 0x7C00 -o bootblock.o bootasm.o bootmain.o
	$(OBJDUMP) -S bootblock.o > bootblock.asm
	$(OBJCOPY) -S -O binary -j .text bootblock.o bootblock
	./sign.pl bootblock

xv6.img: bootblock kernel fs.img 
	dd if=/dev/zero of=xv6.img count=10000
	dd if=bootblock of=xv6.img conv=notrunc
	dd if=kernel of=xv6.img seek=1 conv=notrunc 

kernel: $(OBJS) entry.o initcode kernel.ld 
	$(LD) $(LDFLAGS) -Map kernel.map -T kernel.ld -o kernel entry.o $(OBJS) -b binary initcode
	$(OBJDUMP) -S kernel > kernel.asm
	$(OBJDUMP) -t kernel | sed '1,/SYMBOL TABLE/d; s/ .* / /; /^$$/d' > kernel.sym

mkfs: mkfs.c fs.h
	gcc -Werror -Wall -o mkfs mkfs.c

initcode: initcode.S
	$(CC) $(CFLAGS) -nostdinc -I. -c initcode.S
	$(LD) $(LDFLAGS) -N -e start -Ttext 0 -o initcode.out initcode.o
	$(OBJCOPY) -S -O binary initcode.out initcode
	$(OBJDUMP) -S initcode.o > initcode.asm

clean:
	rm -f *.tex *.dvi *.idx *.aux *.log *.ind *.ilg *.map \
	*.o *.d *.asm *.sym bootblock xv6.img fs.img mkfs kernel \
	*.out initcode _*

ULIB = ulib.o usys.o printf.o umalloc.o

UPROGS = _init _sh _ls _cat

_%: %.o $(ULIB)
	$(LD) $(LDFLAGS) -N -e main -Ttext 0 -o $@ $^
	$(OBJDUMP) -S $@ > $*.asm
	$(OBJDUMP) -t $@ | sed '1,/SYMBOL TABLE/d; s/ .* / /; /^$$/d' > $*.sym

fs.img: mkfs README.md $(UPROGS)
	./mkfs fs.img README.md $(UPROGS)


ifndef CPUS
CPUS := 2
endif

QEMU = qemu-system-i386
QEMUOPTS = -hdb fs.img xv6.img -smp $(CPUS) -m 512 $(QEMUEXTRA)
GDBPORT = $(shell expr `id -u` % 5000 + 25000)
QEMUGDB = -gdb tcp::$(GDBPORT)

qemu: fs.img xv6.img
	$(QEMU) -serial mon:stdio $(QEMUOPTS)

qemu-gdb: fs.img xv6.img .gdbinit
	$(QEMU) -serial mon:stdio $(QEMUOPTS) -S $(QEMUGDB)

.gdbinit: .gdbinit.tmpl
	sed "s/localhost:1234/localhost:$(GDBPORT)/" < $^ > $@
