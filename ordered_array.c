#include "ordered_array.h"
// #include <stdlib.h>
// #include <string.h>
#include "kheap.h"
#include "string.h"

char standard_lessthan_pred(type_t a, type_t b) {
	return (a < b) ? 1 : 0;
}



ordered_array_t create_ordered_array(u32 max_size, lessthan_pred_t less_than) {
	ordered_array_t to_ret;
	to_ret.array = (type_t *) kalloc(sizeof(type_t) * max_size);
	memset((void *)to_ret.array, 0, (u32) sizeof(type_t) * max_size);
	to_ret.size = 0;
	to_ret.max_size = max_size;
	to_ret.less_than = less_than;
	return to_ret;
}

ordered_array_t place_ordered_array(void *addr, u32 max_size, lessthan_pred_t less_than) {
	ordered_array_t to_ret;
	to_ret.array = (type_t *) addr;
	memset((void *) to_ret.array, 0, (u32) sizeof(type_t) * max_size);
	to_ret.max_size = max_size;
	to_ret.size = 0;
	to_ret.less_than = less_than;
	return to_ret;
}

void insert_ordered_array(type_t item, ordered_array_t *array) {
	int i = 0; 
	while (i < array->size && array->less_than(array->array[i], item)) {
		i++;
	}
	if(i == array->size) {
		array->array[array->size++] = item;
	}
	else {
		array->size++;
		while(i < array->size) {
			type_t old = array->array[i];
			array->array[i++] = item;
			item = old;
		}
	}
}

type_t lookup_ordered_array(u32 i, ordered_array_t *array) {
	return array->array[i];
}

void remove_ordered_array(u32 i, ordered_array_t *array) {
	while(i < array->size) {
		array->array[i] = array->array[i+1];
		i++;
	}
	array->size--;
}


